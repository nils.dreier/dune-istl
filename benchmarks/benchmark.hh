#include <iostream>
#include <chrono>
#include <iomanip>
#include <utility>
#include <numeric>

#include <dune/common/classname.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/hybridutilities.hh>

#include <tbb/tbb.h>

void initTBB(int numWorkers = tbb::task_scheduler_init::default_num_threads()){
  static tbb::task_scheduler_init init(numWorkers);
}

void header(){
  std::cout << std::setw(48) << "name"
          << std::setw(16) << "time"
          << std::setw(16) << "iterations"
          << std::setw(16) << "time/it"
          << std::setw(16) << "Gflops/s";
}

template<class NameType>
void print(const NameType& name, double t, size_t flops, size_t it){
  std::stringstream ss; ss  << "\"" << name << "\"";
  std::cout << std::setw(48) << ss.str()
            << std::setw(16) << t
            << std::setw(16) << it
            << std::setw(16) << t/it
            << std::setw(16) << (flops/t)/1e9;
}

template<class Kernel>
void benchmark(const Dune::ParameterTree& config){
  Kernel kernel(config);
  size_t flops_per_it = kernel.flops();
  std::chrono::duration<double> spin_time(config.get("spin_time", 1.0));
  size_t spin_it=0;
  auto t0 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> t;
  do {
    kernel.run();
    spin_it++;
    t = std::chrono::high_resolution_clock::now()-t0;
  }while(t < 0.1*spin_time);
  spin_it *= std::max<size_t>(1,spin_time.count()/t.count());
  t0 = std::chrono::high_resolution_clock::now();
  for(size_t k = 0; k < spin_it; ++k){
    kernel.run();
  }
  std::chrono::duration<double> time = std::chrono::high_resolution_clock::now() -t0;
  size_t flops = spin_it*flops_per_it;
  print(kernel.name(), time.count(),flops, spin_it);
  kernel.printArgs();
  std::cout << std::endl;
}

template<template<size_t> class K, class P>
void benchmark(const Dune::ParameterTree& config,
               const P& p = {}){
  if(!config.get("nohdr", false)){
    header();
    K<1>::printHeader();
    std::cout << std::endl;
  }
  Dune::Hybrid::forEach(p, [&](auto para){
                             constexpr size_t p = para;
                             if constexpr (K<p>::execute)
                               benchmark<K<p>>(config);
                           });
}
