// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_PIPELINEDCG_HH
#define DUNE_ISTL_PIPELINEDCG_HH

#include <dune/istl/scalarproducts.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solverfactory.hh>

namespace Dune{

  template<class X>
  class TwoReductionCG
    : public IterativeSolver<X,X>{
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;

    TwoReductionCG(std::shared_ptr<LinearOperator<X,X> > op, std::shared_ptr<ScalarProduct<X> > sp, std::shared_ptr<Preconditioner<X,X> > prec, const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp, prec, config)
      , residual_ortho_(config.get("residual_ortho", 0.0))
    {}

  private:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::sqrt;
      using std::abs;
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector

      // some local variables
      Block<X> rho, rholast, alpha, sigma, gamma;
      scalar_real_type cond_alpha = 0;
      Future<real_type> defect_future;
      if(residual_ortho_ > 0){
        defect_future = PostProcessFuture<real_type, Block<X>>(_sp->inormalize(b),
                                          [&](Block<X> s){
                                            sigma = s;
                                            return column_norms(s);
                                          });
      }else{
        defect_future = _sp->inorm(b);
      }

      // determine initial search direction
      p = 0;                          // clear correction
      _prec->apply(p,b);               // apply preconditioner
      rholast = _sp->idot(p,b).get();         // orthogonalization
      if(iteration.step(0, defect_future.get())){
        _prec->post(x);
        return;
      }

      // the loop
      int i=1;
      for ( ; i<=_maxit; i++ )
        {
          // minimize in given search direction p
          _op->apply(p,q);             // q=Ap
          alpha = _sp->idot(p,q).get();       // scalar product
          if(residual_ortho_ > 0)
            cond_alpha = scaled_cond(alpha);
          alpha.invert();
          alpha.rightmultiply(rholast);
          alpha.axpy(-1.0, b, q);          // update defect
          if(residual_ortho_>0)
            alpha.rightmultiply(sigma);

          rholast.invert();
          // convergence test and probably reorthogonalization
          if(residual_ortho_*cond_alpha > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())){
            if(_verbose>1)
              std::cout << "=== orthogonalizing residual" << std::endl;
            gamma = _sp->inormalize(b).get();
            sigma.leftmultiply(gamma);
            gamma.transpose();
            rholast.rightmultiply(gamma);
            defect_future = PseudoFuture<real_type>(column_norms(sigma));
          }else if(residual_ortho_>0){
            defect_future = PostProcessFuture<real_type, Block<X>>(_sp->idot(b,b),
                                                                   [&](Block<X> g){
                                                                     g.rightmultiply(sigma);
                                                                     g.transpose();
                                                                     g.rightmultiply(sigma);
                                                                     return sqrt(abs(g.diagonal()));
                                                                   });
          }else{
            defect_future = _sp->inorm(b); // comp defect norm
          }
          alpha.axpy(1.0, x, p);
          // determine new search direction
          q = 0;                      // clear correction
          _prec->apply(q,b);           // apply preconditioner
          rho = _sp->idot(q,b).get();         // orthogonalization
          if(iteration.step(i, defect_future.get()))
            break;
          rholast.rightmultiply(rho);
          rholast.axpy(1.0, q, p);
          std::swap(p,q);
          rholast = rho;              // remember rho for recurrence
        }

      _prec->post(x);                  // postprocess preconditioner
    }

  protected:

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x);
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/sqrt(fx[i][i]);
      }
      fx.leftmultiply(d);
      fx.rightmultiply(d);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(ev[K-1])/abs(ev[0]);
    }

    double residual_ortho_ = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("tworeductioncg", defaultIterativeSolverCreator<Dune::TwoReductionCG>());

  template<class X>
  class GroppCG
    : public IterativeSolver<X,X>{
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;

    GroppCG(std::shared_ptr<LinearOperator<X,X> > op, std::shared_ptr<ScalarProduct<X> > sp, std::shared_ptr<Preconditioner<X,X> > prec, const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp, prec, config)
      , residual_ortho_(config.get("residual_ortho", 0.0))
    {}

  private:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::sqrt;
      using std::abs;
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector
      X v(x);
      X z(x);

      // some local variables
      Block<X> rho, rholast, alpha, sigma, gamma, sigmalast;
      Future<Block<X>> alpha_f, rho_f;
      scalar_real_type cond_alpha = 0;
      Future<real_type> defect_future;
      if(residual_ortho_ > 0){
        defect_future = PostProcessFuture<real_type, Block<X>>(_sp->inormalize(b),
                                          [&](Block<X> s){
                                            sigma = s;
                                            return column_norms(s);
                                          });
      }else{
        defect_future = _sp->inorm(b);
      }

      // determine initial search direction
      p = 0;                          // clear correction
      _prec->apply(p,b);               // apply preconditioner
      z = p;
      _op->apply(p,q);             // q=Ap
      rholast = _sp->idot(p,b).get();         // orthogonalization
      if(iteration.step(0, defect_future.get())){
        _prec->post(x);
        return;
      }

      // the loop
      int i=1;
      for ( ; i<=_maxit; i++ )
        {
          // minimize in given search direction p
          alpha_f = _sp->idot(p,q);       // scalar product
          v = 0.0;
          _prec->apply(v,q);
          alpha = alpha_f.get();
          if(residual_ortho_ > 0)
            cond_alpha = scaled_cond(alpha);
          alpha.invert();
          alpha.rightmultiply(rholast);
          alpha.axpy(-1.0, b, q);          // update defect

          rholast.invert();
          sigmalast = sigma;
          // convergence test and probably reorthogonalization
          if(residual_ortho_*cond_alpha > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())){
            if(_verbose>1)
              std::cout << "=== orthogonalizing residual" << std::endl;
            gamma = _sp->inormalize(b).get();
            sigma.leftmultiply(gamma);
            gamma.transpose();
            rholast.rightmultiply(gamma);
            defect_future = PseudoFuture<real_type>(column_norms(sigma));
            z = 0.0;
            _prec->apply(z,b);           // apply preconditioner
          }else if(residual_ortho_>0){
            defect_future = PostProcessFuture<real_type, Block<X>>(_sp->idot(b,b),
                                                                   [&](Block<X> g){
                                                                     g.rightmultiply(sigma);
                                                                     g.transpose();
                                                                     g.rightmultiply(sigma);
                                                                     return sqrt(abs(g.diagonal()));
                                                                   });
            alpha.axpy(-1.0, z, v);
          }else{
            defect_future = _sp->inorm(b); // comp defect norm
            alpha.axpy(-1.0, z, v);
          }
          if(residual_ortho_>0)
            alpha.rightmultiply(sigmalast);
          alpha.axpy(1.0, x, p);
          // determine new search direction
          rho_f = _sp->idot(z,b);         // orthogonalization
          _op->apply(z, v);
          if(iteration.step(i, defect_future.get())){
            rho_f.wait();
            break;
          }
          rho = rho_f.get();
          rholast.rightmultiply(rho);
          rholast.axpy(1.0, v, q);
          std::swap(q,v);
          v = z;
          rholast.axpy(1.0, v, p);
          std::swap(p,v);
          rholast = rho;              // remember rho for recurrence
        }

      _prec->post(x);                  // postprocess preconditioner
    }

  protected:

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x);
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/sqrt(fx[i][i]);
      }
      fx.leftmultiply(d);
      fx.rightmultiply(d);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(ev[K-1])/abs(ev[0]);
    }

    double residual_ortho_ = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("groppcg", defaultIterativeSolverCreator<Dune::GroppCG>());

  // preconditioner and operator overlaps with communication
  template<class X>
  class GhyselsPipelinedCG
    : public IterativeSolver<X,X>{
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;

    GhyselsPipelinedCG(std::shared_ptr<LinearOperator<X,X> > op, std::shared_ptr<ScalarProduct<X> > sp, std::shared_ptr<Preconditioner<X,X> > prec, const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp, prec, config)
      , residual_ortho_(config.get("residual_ortho", 0.0))
    {}

  private:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::sqrt;
      using std::abs;
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector
      X z(x), u(x), v(x), w(x), s(x), t(x);

      // some local variables
      Block<X> rho, rholast, alpha, sigma, gamma, lambda;
      Future<Block<X>> delta_f;
      scalar_real_type cond_alpha = 0;
      Future<real_type> defect_future;
      if(residual_ortho_ > 0){
        defect_future = PostProcessFuture<real_type, Block<X>>(_sp->inormalize(b),
                                          [&](Block<X> s){
                                            sigma = s;
                                            return column_norms(s);
                                          });
      }else{
        defect_future = _sp->inorm(b);
      }

      // determine initial search direction
      p = 0;                          // clear correction
      _prec->apply(p,b);               // apply preconditioner
      _op->apply(p,q);             // q=Ap
      z = p;
      u = q;
      v = 0.0;
      _prec->apply(v, q);
      _op->apply(v, s);
      auto rholast_f =  _sp->idot(p,b);         // orthogonalization
      alpha = _sp->idot(p,q).get();       // scalar product
      rholast = rholast_f.get();
      if(iteration.step(0, defect_future)){
        _prec->post(x);
        return;
      }

      // the loop
      int i=1;
      for ( ; i<=_maxit; i++ )
        {
          // minimize in given search direction p
          if(residual_ortho_ > 0)
            cond_alpha = scaled_cond(alpha);
          lambda = alpha;
          lambda.invert();
          lambda.rightmultiply(rholast);
          lambda.axpy(-1.0, b, q);          // update defect

          rholast.invert();
          // convergence test and probably reorthogonalization
          if(residual_ortho_*cond_alpha > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())){
            if(_verbose>1)
              std::cout << "=== orthogonalizing residual" << std::endl;
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
            gamma = _sp->inormalize(b).get();
            sigma.leftmultiply(gamma);
            gamma.transpose();
            rholast.rightmultiply(gamma);
            defect_future = PseudoFuture<real_type>(column_norms(sigma));
            z=0.0;
            _prec->apply(z,b);
            _op->apply(z,u);
          }else if(residual_ortho_>0){
            defect_future = PostProcessFuture<real_type, Block<X>>(_sp->idot(b,b),
                                                                   [&](Block<X> g){
                                                                     g.rightmultiply(sigma);
                                                                     g.transpose();
                                                                     g.rightmultiply(sigma);
                                                                     return sqrt(abs(g.diagonal()));
                                                                   });
            lambda.axpy(-1.0, z, v);
            lambda.axpy(-1.0, u, s);
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
          }else{
            defect_future = _sp->inorm(b); // comp defect norm
            lambda.axpy(-1.0, z, v);
            lambda.axpy(-1.0, u, s);
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
          }
          lambda.axpy(1.0, x, p);
          // determine new search direction
          auto rho_f = _sp->idot(z,b);         // orthogonalization
          auto delta_f = _sp->idot(z,u);
          w = 0.0;
          _prec->apply(w, u);
          _op->apply(w, t);
          if(iteration.step(i, defect_future.get())){
            // collectives cant be canceled => we must wait for it
            rho_f.wait();
            delta_f.wait();
            break;
          }
          rho = rho_f.get();
          rholast.rightmultiply(rho);
          rholast.axpy(1.0, w, v);
          std::swap(w,v);
          rholast.axpy(1.0, t, s);
          std::swap(s,t);
          w = z;
          rholast.axpy(1.0, w, p);
          std::swap(p,w);
          w = u;
          rholast.axpy(1.0, w, q);
          std::swap(w,q);
          // compute alpha
          alpha.rightmultiply(rholast);
          alpha.transpose();
          alpha.rightmultiply(rholast);
          alpha.scale(-1.0);
          alpha.add(delta_f.get());

          rholast = rho;              // remember rho for recurrence
        }

      _prec->post(x);                  // postprocess preconditioner
    }

  protected:

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x);
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/sqrt(fx[i][i]);
      }
      fx.leftmultiply(d);
      fx.rightmultiply(d);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(ev[K-1])/abs(ev[0]);
    }

    double residual_ortho_ = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("ghyselspipelinedcg", defaultIterativeSolverCreator<Dune::GhyselsPipelinedCG>());

  // preconditioner and operator overlaps with communication
  template<class X>
  class PartiallyPipelinedCG
    : public IterativeSolver<X,X>{
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;

    PartiallyPipelinedCG(std::shared_ptr<LinearOperator<X,X> > op, std::shared_ptr<ScalarProduct<X> > sp, std::shared_ptr<Preconditioner<X,X> > prec, const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp, prec, config)
      , residual_ortho_(config.get("residual_ortho", 0.0))
    {}

  private:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::sqrt;
      using std::abs;
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector
      X z(x), u(x), v(x), w(x);

      // some local variables
      Block<X> rho, rholast, alpha, sigma, gamma, lambda;
      Future<Block<X>> delta_f;
      scalar_real_type cond_alpha = 0;
      Future<real_type> defect_future;
      if(residual_ortho_ > 0){
        defect_future = PostProcessFuture<real_type, Block<X>>(_sp->inormalize(b),
                                          [&](Block<X> s){
                                            sigma = s;
                                            return column_norms(s);
                                          });
      }else{
        defect_future = _sp->inorm(b);
      }

      // determine initial search direction
      p = 0;                          // clear correction
      _prec->apply(p,b);               // apply preconditioner
      _op->apply(p,q);             // q=Ap
      z = p;
      v = 0.0;
      _prec->apply(v, q);
      auto rholast_f =  _sp->idot(p,b);         // orthogonalization
      alpha = _sp->idot(p,q).get();       // scalar product
      rholast = rholast_f.get();
      if(iteration.step(0, defect_future)){
        _prec->post(x);
        return;
      }

      // the loop
      int i=1;
      for ( ; i<=_maxit; i++ )
        {
          // minimize in given search direction p
          if(residual_ortho_ > 0)
            cond_alpha = scaled_cond(alpha);
          lambda = alpha;
          lambda.invert();
          lambda.rightmultiply(rholast);
          lambda.axpy(-1.0, b, q);          // update defect

          rholast.invert();
          // convergence test and probably reorthogonalization
          if(residual_ortho_*cond_alpha > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())){
            if(_verbose>1)
              std::cout << "=== orthogonalizing residual" << std::endl;
            gamma = _sp->inormalize(b).get();
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
            sigma.leftmultiply(gamma);
            gamma.transpose();
            rholast.rightmultiply(gamma);
            defect_future = PseudoFuture<real_type>(column_norms(sigma));
            z=0.0;
            _prec->apply(z,b);
          }else if(residual_ortho_>0){
            defect_future = PostProcessFuture<real_type, Block<X>>(_sp->idot(b,b),
                                                                   [&](Block<X> g){
                                                                     g.rightmultiply(sigma);
                                                                     g.transpose();
                                                                     g.rightmultiply(sigma);
                                                                     return sqrt(abs(g.diagonal()));
                                                                   });
            lambda.axpy(-1.0, z, v);
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
          }else{
            defect_future = _sp->inorm(b); // comp defect norm
            lambda.axpy(-1.0, z, v);
            if(residual_ortho_>0)
              lambda.rightmultiply(sigma);
          }
          lambda.axpy(1.0, x, p);
          // determine new search direction
          auto rho_f = _sp->idot(z,b);         // orthogonalization
          _op->apply(z,u);
          auto delta_f = _sp->idot(z,u);
          w = 0.0;
          _prec->apply(w, u);
          if(iteration.step(i, defect_future.get())){
            // collectives cant be canceled => we must wait for it
            rho_f.wait();
            delta_f.wait();
            break;
          }
          rho = rho_f.get();
          rholast.rightmultiply(rho);
          rholast.axpy(1.0, u, q);
          std::swap(u,q);
          rholast.axpy(1.0, w, v);
          std::swap(w,v);
          u = z;
          rholast.axpy(1.0, u, p);
          std::swap(p,u);
          // compute alpha
          alpha.rightmultiply(rholast);
          alpha.transpose();
          alpha.rightmultiply(rholast);
          alpha.scale(-1.0);
          alpha.add(delta_f.get());

          rholast = rho;              // remember rho for recurrence
        }

      _prec->post(x);                  // postprocess preconditioner
    }

  protected:

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x);
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/sqrt(fx[i][i]);
      }
      fx.leftmultiply(d);
      fx.rightmultiply(d);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(ev[K-1])/abs(ev[0]);
    }

    double residual_ortho_ = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("partiallypipelinedcg", defaultIterativeSolverCreator<Dune::PartiallyPipelinedCG>());

  template<class X>
  class OneReductionCG
    : public IterativeSolver<X,X>{
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;

    OneReductionCG(std::shared_ptr<LinearOperator<X,X> > op, std::shared_ptr<ScalarProduct<X> > sp, std::shared_ptr<Preconditioner<X,X> > prec, const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp, prec, config)
      , residual_ortho_(config.get("residual_ortho", 0.0))
    {}

  private:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::sqrt;
      using std::abs;
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector
      X z(x), u(x);

      // some local variables
      Block<X> rho, rholast, alpha, sigma, gamma, lambda;
      Future<Block<X>> delta_f;
      scalar_real_type cond_alpha = 0;
      Future<real_type> defect_future;
      if(residual_ortho_ > 0){
        defect_future = PostProcessFuture<real_type, Block<X>>(_sp->inormalize(b),
                                          [&](Block<X> s){
                                            sigma = s;
                                            return column_norms(s);
                                          });
      }else{
        defect_future = _sp->inorm(b);
      }

      // determine initial search direction
      p = 0;                          // clear correction
      _prec->apply(p,b);               // apply preconditioner
      _op->apply(p,q);             // q=Ap
      auto rholast_f =  _sp->idot(p,b);         // orthogonalization
      alpha = _sp->idot(p,q).get();       // scalar product
      rholast = rholast_f.get();
      if(iteration.step(0, defect_future)){
        _prec->post(x);
        return;
      }

      // the loop
      int i=1;
      for ( ; i<=_maxit; i++ )
        {
          // minimize in given search direction p
          if(residual_ortho_ > 0)
            cond_alpha = scaled_cond(alpha);
          lambda = alpha;
          lambda.invert();
          lambda.rightmultiply(rholast);
          lambda.axpy(-1.0, b, q);          // update defect
          if(residual_ortho_>0)
            lambda.rightmultiply(sigma);

          rholast.invert();
          // convergence test and probably reorthogonalization
          if(residual_ortho_*cond_alpha > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())){
            if(_verbose>1)
              std::cout << "=== orthogonalizing residual" << std::endl;
            gamma = _sp->inormalize(b).get();
            sigma.leftmultiply(gamma);
            gamma.transpose();
            rholast.rightmultiply(gamma);
            defect_future = PseudoFuture<real_type>(column_norms(sigma));
          }else if(residual_ortho_>0){
            defect_future = PostProcessFuture<real_type, Block<X>>(_sp->idot(b,b),
                                                                   [&](Block<X> g){
                                                                     g.rightmultiply(sigma);
                                                                     g.transpose();
                                                                     g.rightmultiply(sigma);
                                                                     return sqrt(abs(g.diagonal()));
                                                                   });
          }else{
            defect_future = _sp->inorm(b); // comp defect norm
          }
          lambda.axpy(1.0, x, p);
          // determine new search direction
          z = 0;                      // clear correction
          _prec->apply(z,b);           // apply preconditioner
          auto rho_f = _sp->idot(z,b);         // orthogonalization
          _op->apply(z,u);
          auto delta_f = _sp->idot(z,u);
          if(iteration.step(i, defect_future.get())){
            // collectives cant be canceled => we must wait for it
            rho_f.wait();
            delta_f.wait();
            break;
          }
          rho = rho_f.get();
          rholast.rightmultiply(rho);
          rholast.axpy(1.0, z, p);
          rholast.axpy(1.0, u, q);
          std::swap(p,z);
          std::swap(u,q);
          // compute alpha
          alpha.rightmultiply(rholast);
          alpha.transpose();
          alpha.rightmultiply(rholast);
          alpha.scale(-1.0);
          alpha.add(delta_f.get());

          rholast = rho;              // remember rho for recurrence
        }

      _prec->post(x);                  // postprocess preconditioner
    }

  protected:

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x);
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/sqrt(fx[i][i]);
      }
      fx.leftmultiply(d);
      fx.rightmultiply(d);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(ev[K-1])/abs(ev[0]);
    }

    double residual_ortho_ = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("onereductioncg", defaultIterativeSolverCreator<Dune::OneReductionCG>());
}

#endif
