// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include "config.h"

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/io.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/istl/blockproducts.hh>
#include <dune/istl/overlappingschwarz.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/pipelinedcg.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/paamg/test/anisotropic.hh>
#include <dune/istl/paamg/pinfo.hh>
#include <dune/istl/paamg/amg.hh>
#include "laplacian.hh"

#include <iterator>

template<class X>
void fillRandom(X& x){
  using Dune::Simd::lane;
  for(size_t i=0;i<x.size();++i){
    for(size_t l=0;l<Dune::Simd::lanes(x[i]);++l){
      lane(l, x[i]) = drand48();
    }
  }
}

template<template<class> class Solver, class X>
void test(const Dune::ParameterTree& config,
          X& x,
          X& b,
          std::shared_ptr<Dune::LinearOperator<X,X>> op,
          std::shared_ptr<Dune::ScalarProduct<X>> sp,
          std::shared_ptr<Dune::Preconditioner<X,X>> prec){
  Dune::Timer watch;
  int maxit = config.get("maxit", 1000);
  int verbose = config.get("verbose", 1);
  double reduction = config.get("reduction", 1e-5);
  Dune::InverseOperatorResult res;
  x=1;
  op->apply(x, b);
  X b_orig(b);
  fillRandom(x);
  op->applyscaleadd(-1, x, b);
  auto def0 = sp->norm(b);
  b = b_orig;
  Solver<X> solver0(op, sp, prec, reduction,maxit,verbose);
  if(solver0.name() == config.get("solver", solver0.name())){
    solver0.apply(x,b, res);

    // check solution
    op->applyscaleadd(-1, x, b_orig);
    if( Dune::Simd::anyTrue(sp->norm(b_orig) > 2.0*reduction*def0))
      DUNE_THROW(Dune::Exception, "Solver did not converge!" << sp->norm(b_orig)
                 << "  /  " << def0);
  }
}

int main(int argc, char** argv)
{
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);
  const int BS=1;

  Dune::ParameterTree config;
  Dune::ParameterTreeParser::readOptions(argc, argv, config);
  try{
    Dune::ParameterTreeParser::readINITree(config.get("config", "config.ini"), config, false);
  }catch(...){}

  int N=config.get("N", 30);

  int rank = mpihelper.rank();
  int verbose = config.get("verbose",1);
  verbose = (config.get("verbose_rank", 0) == rank)?verbose:0;
  config["verbose"] = std::to_string(verbose);

  typedef int GlobalId;
  typedef Dune::OwnerOverlapCopyCommunication<GlobalId> Communication;
  typedef Dune::RedistributeInformation<Communication> RedistInfo;

  //typedef Dune::FieldMatrix<double,BS,BS> MatrixBlock;
  typedef Dune::BCRSMatrix<double> BCRSMat;
  typedef Dune::LoopSIMD<double, 4> VectorBlock;
  typedef Dune::BlockVector<VectorBlock> BVector;
  // typedef Dune::MatrixAdapter<BCRSMat,BVector,BVector> Operator;
  typedef Dune::OverlappingSchwarzOperator<BCRSMat, BVector, BVector, Communication> Operator;
  // typedef Dune::OverlappingSchwarzScalarProduct<BVector, Communication> ScalarProductType;
  typedef Dune::ParallelBlockProduct<BVector, Communication,2> ScalarProductType;

  BCRSMat mat;

  auto world = mpihelper.getCommunication();
  Communication oocomm(world);

  int n;
  mat = setupAnisotropic2d<double>(N, oocomm.indexSet(), mpihelper.getCommunication(), &n, 1);

  oocomm.remoteIndices().template rebuild<false>();

  std::shared_ptr<Operator> fop = std::make_shared<Operator>(mat, oocomm);
  std::shared_ptr<ScalarProductType> sp = std::make_shared<ScalarProductType>(oocomm, Dune::SolverCategory::overlapping);

  BVector b(mat.N()), x(mat.M());
  x = 1;
  mat.mv(x,b);
  x = 0;


  std::shared_ptr<Dune::Preconditioner<BVector,BVector>> prec0
    // = std::make_shared<Dune::SeqJac<BCRSMat,BVector,BVector>>(mat, 1,1.0);
    = std::make_shared<Dune::ParSSOR<BCRSMat,BVector,BVector, Communication>>(mat, 1,1.0, oocomm);

  for(int r=0; r < config.get("repeat", 1); ++r){
    test<Dune::CGSolver, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::TwoReductionCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::GroppCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::OneReductionCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::PartiallyPipelinedCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::GhyselsPipelinedCG, BVector>(config, x, b, fop, sp, prec0);
  }

  return 0;
}
