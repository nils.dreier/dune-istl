// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_BLOCKBICGSTAB_HH
#define DUNE_ISTL_BLOCKBICGSTAB_HH

#include <dune/common/simd/simd.hh>

#include <dune/istl/scalarproducts.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solverfactory.hh>
#include <dune/istl/simdtsqr.hh>

namespace Dune {
  template<class X>
  class BlockBiCGSTAB : public IterativeSolver<X, X>
  {
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type=Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

    //using IterativeSolver<X, X>::IterativeSolver;

    BlockBiCGSTAB(const std::shared_ptr<LinearOperator<X,X>>& op,
                   const std::shared_ptr<ScalarProduct<X>>& sp,
                   const std::shared_ptr<Preconditioner<X,X>>& prec,
                   const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp,prec, config)
      , _residual_ortho(config.get("residual_ortho", 0.0))
    {}

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::abs;

      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner
      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      real_type norm;
      Block<X> sigma;
      if(_residual_ortho > 0){
        sigma = _sp->inormalize(b).get();
        norm = column_norms(sigma);
      }else{
        norm = _sp->norm(b);
      }

      if(iteration.step(0, norm)){
        _prec->post(x);
        return;
      }
      X p(b);
      p=0.0;
      _prec->apply(p, b);

      X rt0(p), v(p), q(p), z(b), t(b), u(b);
      Block<X> rho, alpha, beta, gamma, rho_sigma;

      for(int it = 1; it < _maxit; ++it){
        _op->apply(p, q);
        auto alpha_future = _sp->idot(rt0, q);
        rho = _sp->idot(rt0, b).get();
        alpha = alpha_future.get();
        alpha.invert();
        rho.leftmultiply(alpha);
        z=0.0;
        _prec->apply(z,q);
        rho.axpy(-1.0, b, q); // update S
        rho_sigma = rho;
        if(_residual_ortho>0){
          rho_sigma.rightmultiply(sigma);
        }
        rho_sigma.axpy(1.0, x, p); // update X
        gamma = _sp->idot(b,b).get();
        auto residual_cond = scaled_cond(gamma);
        if(_residual_ortho>0){
          gamma.rightmultiply(sigma);
          gamma.transpose();
          gamma.rightmultiply(sigma);
        }
        FieldMatrix<scalar_field_type,K,K> fm(gamma);
        for(size_t l=0;l<K;++l){
          using std::sqrt;
          using std::abs;
          Simd::lane(l,norm)=abs(sqrt(fm[l][l]));
        }
        if(iteration.step(it, norm))
          break;
        // ortho
        if(_residual_ortho*residual_cond > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())
           || (isNaN(residual_cond) && _residual_ortho>0)){
          if(_verbose>1)
            std::cout << "=== orthogonalizing residual" << std::endl;
          gamma = _sp->inormalize(b).get();
          sigma.leftmultiply(gamma);
          t = 0.0;
          _prec->apply(t, b);
        }else{
          t = v;
          rho.axpy(-1.0, t, z);
        }
        _op->apply(t,u);
        scalar_field_type omega = frobenius_product(u,b)/frobenius_product(u,u);
        if(_residual_ortho>0){
          sigma.axpy(omega, x, t);
        }else
          x.axpy(omega, t);
        b.axpy(-omega, u);
        beta = _sp->idot(rt0, u).get();
        beta.leftmultiply(alpha);
        v=0.0;
        _prec->apply(v, b);
        p.axpy(-omega, z);
        z=v;
        beta.axpy(-1.0, z, p);
        std::swap(p,z);
      }

      _prec->post(x);
    }

    scalar_field_type frobenius_product(const X& x, const X& y){
      using Simd::lane;
      using Simd::lanes;
      auto dot = _sp->idot(x,y).get().diagonal();
      scalar_field_type sum = 0;
      for(size_t i = 0; i < lanes(dot); ++i)
        sum += lane(i, dot);
      return sum;
    }

    scalar_real_type cond(const FieldMatrix<scalar_real_type, K,K>& x){
      using std::sqrt;
      FieldMatrix<scalar_real_type, K,K> fx(x);
      // transpose
      for(size_t i=0;i<K;++i){
        for(size_t j=i+1; j<K;++j){
          std::swap(fx[i][j], fx[j][i]);
        }
      }
      fx.rightmultiply(x);

      FieldVector<scalar_real_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return sqrt(ev[K-1])/sqrt(ev[0]);
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x), fx2;
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/fx[i][i];
      }
      fx.rightmultiply(d);
      fx2 = fx;
      // tranpose
      for(size_t i=0;i<K;++i){
        for(size_t j=i+1; j<K;++j){
          std::swap(fx2[i][j], fx2[j][i]);
        }
      }
      fx.leftmultiply(fx2);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(sqrt(ev[K-1]))/abs(sqrt(ev[0]));
    }

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

  protected:
    double _residual_ortho = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
    bool _initial_deflation = false;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("blockbicgstab", defaultIterativeSolverCreator<Dune::BlockBiCGSTAB>());


  template<class X>
  class PipelinedBlockBiCGSTAB : public IterativeSolver<X, X>
  {
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type=Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();

    //using IterativeSolver<X, X>::IterativeSolver;

    PipelinedBlockBiCGSTAB(const std::shared_ptr<LinearOperator<X,X>>& op,
                   const std::shared_ptr<ScalarProduct<X>>& sp,
                   const std::shared_ptr<Preconditioner<X,X>>& prec,
                   const ParameterTree& config)
      : IterativeSolver<X,X>(op,sp,prec, config)
      , _residual_ortho(config.get("residual_ortho", 0.0))
    {}

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      using std::abs;

      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner
      _op->applyscaleadd(-1,x,b);  // overwrite b with defect

      real_type norm;
      Block<X> sigma;
      if(_residual_ortho > 0){
        sigma = _sp->inormalize(b).get();
        norm = column_norms(sigma);
      }else{
        norm = _sp->norm(b);
      }

      if(iteration.step(0, norm)){
        _prec->post(x);
        return;
      }
      X p(b);
      p=0.0;
      _prec->apply(p, b);

      X rt0(p), v(p), q(p), z(b), t(b), u(b), w(b);
      Block<X> rho, alpha, beta, gamma, rho_sigma;

      for(int it = 1; it < _maxit; ++it){
        _op->apply(p, q);
        auto alpha_future = _sp->idot(rt0, q);
        auto rho_future = _sp->idot(rt0, b);
        z=0.0;
        _prec->apply(z,q);
        alpha = alpha_future.get();
        alpha.invert();
        rho = rho_future.get();
        rho.leftmultiply(alpha);
        rho.axpy(-1.0, b, q); // update S
        rho_sigma = rho;
        if(_residual_ortho>0){
          rho_sigma.rightmultiply(sigma);
        }
        auto gamma_future = _sp->idot(b,b);
        rho_sigma.axpy(1.0, x, p); // update X
        gamma = gamma_future.get();
        auto residual_cond = scaled_cond(gamma);
        if(_residual_ortho>0){
          gamma.rightmultiply(sigma);
          gamma.transpose();
          gamma.rightmultiply(sigma);
        }
        FieldMatrix<scalar_field_type,K,K> fm(gamma);
        for(size_t l=0;l<K;++l){
          using std::sqrt;
          using std::abs;
          Simd::lane(l,norm)=abs(sqrt(fm[l][l]));
        }
        if(iteration.step(it, norm))
          break;
        // ortho
        if(_residual_ortho*residual_cond > 1.0/std::sqrt(std::numeric_limits<scalar_real_type>::epsilon())
           || (isNaN(residual_cond) && _residual_ortho>0)){
          if(_verbose>1)
            std::cout << "=== orthogonalizing residual" << std::endl;
          gamma = _sp->inormalize(b).get();
          sigma.leftmultiply(gamma);
          t = 0.0;
          _prec->apply(t, b);
        }else{
          t = v;
          rho.axpy(-1.0, t, z);
        }
        _op->apply(t,u);
        auto omega1 = frobenius_product(u,b);
        auto omega2 = frobenius_product(u,u);
        auto beta_future = _sp->idot(rt0, u);
        w = 0.0;
        _prec->apply(w, u);
        scalar_field_type omega = omega1.get()/omega2.get();
        if(_residual_ortho>0){
          sigma.axpy(omega, x, t);
        }else
          x.axpy(omega, t);
        b.axpy(-omega, u);
        v=t;
        v.axpy(-omega, w);
        p.axpy(-omega, z);
        z=v;
        beta = beta_future.get();
        beta.leftmultiply(alpha);
        beta.axpy(-1.0, z, p);
        std::swap(p,z);
      }

      _prec->post(x);
    }

    Future<scalar_field_type> frobenius_product(const X& x, const X& y){
      using Simd::lane;
      using Simd::lanes;
      auto dot = _sp->idot(x,y);
      return PostProcessFuture<scalar_field_type, Block<X>>(std::move(dot),
          [](const Block<X>& x){
            auto dia = x.diagonal();
            scalar_field_type sum = 0;
            for(size_t i = 0; i < lanes(dia); ++i)
              sum += lane(i, dia);
            return sum;
          });
    }

    scalar_real_type cond(const FieldMatrix<scalar_real_type, K,K>& x){
      using std::sqrt;
      FieldMatrix<scalar_real_type, K,K> fx(x);
      // transpose
      for(size_t i=0;i<K;++i){
        for(size_t j=i+1; j<K;++j){
          std::swap(fx[i][j], fx[j][i]);
        }
      }
      fx.rightmultiply(x);

      FieldVector<scalar_real_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return sqrt(ev[K-1])/sqrt(ev[0]);
    }

    scalar_real_type scaled_cond(Block<X>& x){
      using std::sqrt;
      using std::abs;
      FieldMatrix<scalar_field_type, K,K> fx(x), fx2;
      //scale
      FieldMatrix<scalar_field_type, K,K> d = 0.0;
      for(size_t i=0;i<K;++i){
        d[i][i] = scalar_field_type(1.0)/fx[i][i];
      }
      fx.rightmultiply(d);
      fx2 = fx;
      // tranpose
      for(size_t i=0;i<K;++i){
        for(size_t j=i+1; j<K;++j){
          std::swap(fx2[i][j], fx2[j][i]);
        }
      }
      fx.leftmultiply(fx2);

      FieldVector<scalar_field_type, K> ev;
      FMatrixHelp::eigenValues(fx,ev);
      return abs(sqrt(ev[K-1]))/abs(sqrt(ev[0]));
    }

    real_type column_norms(Block<X>& x){
      x.transpose();
      FieldMatrix<scalar_field_type, K,K> fx(x);
      x.transpose();
      real_type n = 0.0;
      for(size_t i=0;i<K;++i){
        Simd::lane(i,n) = fx[i].two_norm();
      }
      return n;
    }

  protected:
    double _residual_ortho = 0.0;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
    bool _initial_deflation = false;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("pipelinedblockbicgstab", defaultIterativeSolverCreator<Dune::PipelinedBlockBiCGSTAB>());
} // end namespace Dune

#endif
