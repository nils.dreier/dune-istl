#!/bin/bash

# This script downloads a matrix and (if exists) a RHS from the
# SuiteSparse Matrix Collection (sparse.tamu.edu) and solves it using
# solve_mm.

GROUP=$1
MATRIX=$2

shift
shift

if [ ! -f ${MATRIX}.tar.gz ]; then
    wget https://sparse.tamu.edu/MM/$GROUP/$MATRIX.tar.gz
fi
if [ ! -f ${MATRIX}/${MATRIX}.mtx ]; then
    tar -xzf $MATRIX.tar.gz
fi

ARGS=$@

if [ -f "${MATRIX}/${MATRIX}_b.mtx" ]; then
    ARGS="$ARGS -rhs ${MATRIX}/${MATRIX}_b.mtx"
else
    ARGS="$ARGS -random_rhs 1"
fi

echo "Calling ./solve_mm -matrix ${MATRIX}/${MATRIX}.mtx $ARGS"
./solve_mm -matrix ${MATRIX}/${MATRIX}.mtx $ARGS
