#!/bin/bash

matrix="bcsstk18/bcsstk18.mtx"

Ps="1 2 4 8 16 32 64 128 256"

params="-solver.frobeniusnorm 1 -check_residual 0"

for P in $Ps
do
    echo "P=$P"
    echo "residual_ortho=0"
    ./solve_mm -matrix $matrix -solver.p ${P} -solver.residual_ortho 0
    echo "residual_ortho=1"
    ./solve_mm -matrix $matrix -solver.p ${P} -solver.residual_ortho 1
done
