#include <config.h>
#include <iostream>
#include <numeric>

#include <dune/common/alignedallocator.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parametertree.hh>

#include <dune/common/simd/loop.hh>
#if HAVE_DUNE_VECTORCLASS
#include <dune/vectorclass/vecarray.hh>
#endif
#if HAVE_VC
#include <dune/common/simd/vc.hh>
#endif

#if HAVE_STDSIMD
#include <dune/common/simd/stdsimd.hh>
#endif

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include "benchmark.hh"

using namespace Dune;
using namespace Dune::Simd;

template<class M>
void build2DFD(M& m, size_t N){
  size_t offset = std::sqrt(N);
  m.setSize(N, N, 5*N);
  m.setBuildMode(M::row_wise);
  for(auto i = m.createbegin(); i != m.createend(); ++i){
    auto idx = i.index();
    i.insert(idx);
    if(idx > 0)
      i.insert(idx-1);
    if(idx < N)
      i.insert(idx+1);
    if(idx > offset)
      i.insert(idx-offset);
    if(idx < N-offset)
      i.insert(idx+offset);
  }
  // fill
  for (auto i = m.begin(); i != m.end(); ++i){
    for(auto j=i->begin(); j!= i->end(); ++j){
      *j = i.index()==j.index()?4.0:-1.0;
    }
  }
}

template<class M>
void build2DQ1(M& m, size_t N){
  size_t offset = std::sqrt(N);
  m.setSize(N, N, 9*N);
  m.setBuildMode(M::row_wise);
  for(auto i = m.createbegin(); i != m.createend(); ++i){
    auto idx = i.index();
    i.insert(idx);
    if(idx > 0)
      i.insert(idx-1);
    if(idx < N)
      i.insert(idx+1);
    if(idx > offset)
      i.insert(idx-offset);
    if(idx > offset-1)
      i.insert(idx-offset+1);
    if(idx > offset+1)
      i.insert(idx-offset-1);
    if(idx < N-offset)
      i.insert(idx+offset);
    if(idx < N-offset-1)
      i.insert(idx+offset+1);
    if(idx < N-offset+1)
      i.insert(idx+offset-1);
  }
  // fill
  for (auto i = m.begin(); i != m.end(); ++i){
    for(auto j=i->begin(); j!= i->end(); ++j){
      *j = i.index()==j.index()?8.0:-1.0;
    }
  }
}

template<class M>
void build3DQ1(M& m, size_t N){
  size_t offset = std::cbrt(N);
  m.setSize(N, N, 27*N);
  m.setBuildMode(M::row_wise);
  for(auto i = m.createbegin(); i != m.createend(); ++i){
    for(int d1 = -1; d1 <=1; ++d1)
      for(int d2 = -1; d2 <=1; ++d2)
        for(int d3 = -1; d3 <=1; ++d3){
          int idx = i.index() + d1 + d2*offset + d3*offset*offset;
          if(idx > 0 && idx < N)
            i.insert(idx);
        }
  }
  // fill
  for (auto i = m.begin(); i != m.end(); ++i){
    for(auto j=i->begin(); j!= i->end(); ++j){
      *j = i.index()==j.index()?26.0:-1.0;
    }
  }
}

template<class SIMD>
struct BopBenchmark{
  constexpr static size_t K = lanes<SIMD>();
  constexpr static bool execute = true;
  typedef Dune::BlockVector<SIMD, Dune::AlignedAllocator<SIMD>> Vector;
  typedef Dune::BCRSMatrix<double> Matrix;
  size_t n;
  Matrix mat;
  Vector x, y;

  BopBenchmark(const ParameterTree& config)
    : x(n)
    , y(n)
    , n(config.get("N", 1000000)){
    std::iota(x.begin(), x.end(), -1.2);
    std::fill(y.begin(), y.end(), -1.5);
    std::string pattern = config.get("pattern", "2DFD");
    if(pattern == "2DFD")
      build2DFD(mat, n);
    else if(pattern == "2DQ1")
      build2DQ1(mat, n);
    else if(pattern == "3DQ1")
      build3DQ1(mat, n);
    else
      DUNE_THROW(Dune::Exception, "Unknown pattern");
  }

  ~BopBenchmark(){
    asm volatile ("" :: "m" (y));
  }

  void run(){
    mat.mv(x,y);
  }

  size_t flops() const{
    return 2*K*mat.nonzeroes();
  }

  std::string name() const{
    std::string simd = className<SIMD>();
    size_t pos = simd.find("LoopSIMD");
    if(pos != std::string::npos){
      size_t pos2 = simd.find(",",pos);
      simd = "LoopSIMD(" + simd.substr(pos+9, pos2-pos-9) + ")";
    }
    return "BOPbenchmark("+simd+")";
  }


  static void printHeader(){
    std::cout << std::setw(16) << "K"
              << std::setw(16) << "N"
              << std::setw(16) << "NNZ";
  }

  void printArgs() const{
    std::cout << std::setw(16) << K
              << std::setw(16) << x.size()
              << std::setw(16) << mat.nonzeroes();
  }
};


template<size_t K>
using Benchmark_VecArray = BopBenchmark<VecArray<double,K>>;

template<size_t K>
using Benchmark_LoopSIMD = BopBenchmark<LoopSIMD<double,K,std::min(64ul,sizeof(double)*K)>>;

typedef std::index_sequence<1,2,4,8,16,24,32,40,48,56,64> Ks;

int main(int argc, char** argv){
  ParameterTree config;
  ParameterTreeParser::readOptions(argc, argv, config);
  initTBB(config.get("workers", tbb::task_scheduler_init::default_num_threads()));
  std::string simd = config.get("simd", "VectorClass");
  if(simd == "VectorClass"){
    benchmark<Benchmark_VecArray>(config, Ks{});
  }else if(simd == "LoopSIMD"){
    benchmark<Benchmark_LoopSIMD>(config, Ks{});
  }
  return 0;
}
