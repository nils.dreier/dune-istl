// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_SSTEPCG_HH
#define DUNE_ISTL_SSTEPCG_HH

namespace Dune{

  namespace Impl {
    // Matrix of Blocks
    template<class X>
    class MoB {
      std::vector<Block<X>> _data;
      size_t _s;
      using field_type = typename X::field_type;
      using scalar_field_type = Simd::Scalar<field_type>;

    public:
      MoB(size_t s)
        : _data(s*s)
        , _s(s)
      {}

      Block<X>* operator[](size_t i){
        return &_data[i*_s];
      }

      const Block<X>* operator[](size_t i) const{
        return &_data[i*_s];
      }

      void transpose(){
        // transpose diagonal
        for(size_t i=0; i<_s;++i){
          _data[i*_s+i].transpose();
        }
        //swap and transpose off-diagonal
        for(size_t i=0;i<_s;++i){
          for(size_t j=i+1; j<_s;++j){
            _data[i*_s+j].transpose();
            _data[j*_s+i].transpose();
            std::swap(_data[j*_s+i], _data[i*_s+j]);
          }
        }
      }

      void invert(){
        // for stability reasons we do this by copy to DenseMatrix,
        // later we probably want to implement it more efficiently
        constexpr size_t k = Simd::lanes<field_type>();
        DynamicMatrix<scalar_field_type> mat(_s*k, _s*k);
        for(size_t i=0; i<_s;++i){
          for(size_t j=0; j<_s; ++j){
            FieldMatrix<scalar_field_type, k, k> bmat(_data[i*_s+j]);
            for(size_t n=0; n<k;++n){
              for(size_t m=0;m<k;++m){
                mat[i*k+n][j*k+m] = bmat[n][m];
              }
            }
          }
        }
        mat.invert();
        for(size_t i=0; i<_s;++i){
          for(size_t j=0; j<_s; ++j){
            FieldMatrix<scalar_field_type, k, k> bmat = 0;
            for(size_t n=0; n<k;++n){
              for(size_t m=0;m<k;++m){
                bmat[n][m] = mat[i*k+n][j*k+m];
              }
            }
            _data[i*_s + j] = bmat;
          }
        }
      }

      void mv(std::vector<Block<X>>& x){
        std::vector<Block<X>> b(x);
        for(size_t i=0;i<_s;++i){
          x[i] = b[0];
          x[i].leftmultiply((*this)[i][0]);
          for(size_t j=1;j<_s;++j){
            Block<X> tmp = b[j];
            tmp.leftmultiply((*this)[i][j]);
            x[i].add(tmp);
          }
        }
      }

      void rightmultiply(const MoB& other){
        MoB tmp(*this);
        for(size_t i=0;i<_s;++i){
          for(size_t j=0;j<_s;++j){
            (*this)[i][j] = tmp[i][0];
            (*this)[i][j].rightmultiply(other[0][j]);
            for(size_t s=1;s<_s;++s){
              Block<X> tmp_block = tmp[i][s];
              tmp_block.rightmultiply(other[s][j]);
              (*this)[i][j].add(tmp_block);
            }
          }
        }
      }

      void add(scalar_field_type a, const MoB& other){
        for(size_t i=0;i<_s*_s;++i){
          Block<X> tmp(other._data[i]);
          tmp.scale(a);
          _data[i].add(tmp);
        }
      }
    };
  }

  template<class X>
  class SStepCGSolver : public IterativeSolver<X,X> {
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;

    // copy base class constructors
    using IterativeSolver<X,X>::IterativeSolver;
  protected:
    using typename IterativeSolver<X,X>::scalar_real_type;
    using MoB = Impl::MoB<X>;

  public:
    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    SStepCGSolver(LinearOperator<X,X>& op,
                  Preconditioner<X,X>& prec,
                  const ParameterTree& configuration)
      : IterativeSolver<X,X>(op,prec,configuration)
      , steps(configuration.get("steps", 2))
    {}

    SStepCGSolver(LinearOperator<X,X>& op,
                  ScalarProduct<X>& sp,
                  Preconditioner<X,X>& prec,
                  const ParameterTree& configuration)
      : IterativeSolver<X,X>(op,sp,prec,configuration)
      , steps(configuration.get("steps", 2))
    {}

    SStepCGSolver(std::shared_ptr<LinearOperator<X,X>> op,
                  std::shared_ptr<Preconditioner<X,X>> prec,
                  const ParameterTree& configuration)
      : IterativeSolver<X,X>(op,prec,configuration)
      , steps(configuration.get("steps", 2))
    {}

    SStepCGSolver(std::shared_ptr<LinearOperator<X,X>> op,
                  std::shared_ptr<ScalarProduct<X>> sp,
                  std::shared_ptr<Preconditioner<X,X>> prec,
                  const ParameterTree& configuration)
      : IterativeSolver<X,X>(op,sp,prec,configuration)
      , steps(configuration.get("steps", 2))
    {}

    virtual void apply(X& x, X& b, InverseOperatorResult& res) override {
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      _op->applyscaleadd(-1,x,b);          // overwrite b with defect

      auto def = _sp->inorm(b); // compute norm
      if(iteration.step(0, def.get())){
        _prec->post(x);
        return;
      }

      // todo initial deflation

      std::vector<X> p(steps);
      std::fill(p.begin(), p.end(), x);
      std::vector<X> q(p), u(p), z(p);
      p[0]=0.0;
      _prec->apply(p[0], b);
      _op->apply(p[0], q[0]);
      for(unsigned int s=1;s<steps;++s){
        p[s]=0.0;
        _prec->apply(p[s], q[s-1]);
        _op->apply(p[s], q[s]);
      }

      std::vector<Block<X>> alpha(steps);
      MoB pq_inv(steps), beta(steps);
      init_scalars(p,q,b,alpha,pq_inv);
      for (int i = steps ; i<=_maxit; i+=steps ){
        for(unsigned int s=0;s<steps;++s){
          alpha[s].axpy(1.0, x, p[s]);
          alpha[s].axpy(-1.0, b, q[s]);
        }
        def=_sp->inorm(b);               // comp defect norm
        if(iteration.step(i, def.get()))
          break;
        z[0]=0.0;
        _prec->apply(z[0], b);
        _op->apply(z[0], u[0]);
        for(unsigned int s=1;s<steps;++s){
          z[s]=0.0;
          _prec->apply(z[s], u[s-1]);
          _op->apply(z[s], u[s]);
        }
        scalar_work(z,u,p,b,pq_inv, beta, alpha);
        //orthogonalize
        for(size_t s0=0; s0<steps;++s0){
          for(size_t s1=0; s1<steps; ++s1){
            beta[s0][s1].axpy(-1.0, z[s1], p[s0]);
            beta[s0][s1].axpy(-1.0, u[s1], q[s0]);
          }
        }
        std::swap(z,p);
        std::swap(u,q);
      }
    }

    void init_scalars(const std::vector<X>& p,
                      const std::vector<X>& q,
                      const X& r,
                      std::vector<Block<X>>& alpha,
                      MoB& pq_inv){
      std::vector<Future<Block<X>>> futures_pr(steps);
      std::vector<std::vector<Future<Block<X>>>> futures_pq(steps);
      for(size_t i=0; i<steps; ++i){
        futures_pr[i] = _sp->idot(p[i],r);
        futures_pq[i].resize(steps);
        for(size_t j=0;j<steps;++j){
          futures_pq[i][j] = _sp->idot(p[i], q[j]);
        }
      }
      for(size_t i=0;i<steps;++i){
        alpha[i] = futures_pr[i].get();
        for(size_t j=0;j<steps;++j){
          pq_inv[i][j] = futures_pq[i][j].get();
        }
      }
      pq_inv.invert();
      pq_inv.mv(alpha);
    }

    void scalar_work(const std::vector<X>& z,
                     const std::vector<X>& u,
                     const std::vector<X>& p,
                     const X& r,
                     MoB& pq_inv,
                     MoB& beta, //inout
                     std::vector<Block<X>>& alpha // out
                     ){
      assert(z.size()==steps);
      assert(u.size()==steps);
      assert(p.size()==steps);
      assert(alpha.size()==steps);
      std::vector<std::vector<Future<Block<X>>>> futures_zu(steps);
      std::vector<std::vector<Future<Block<X>>>> futures_pu(steps);
      std::vector<Future<Block<X>>> futures_zr(steps);
      MoB pu(steps);
      // zu
      for(size_t i=0; i<steps;++i){
        futures_zu[i].resize(steps);
        for(size_t j=0; j<steps;++j){
          futures_zu[i][j] = _sp->idot(z[i],u[j]);
        }
      }
      // zr
      for(size_t i=0; i<steps;++i){
        futures_zr[i] = _sp->idot(z[i], r);
      }
      // pu
      for(size_t i=0; i<steps;++i){
        futures_pu[i].resize(steps);
        for(size_t j=0; j<steps;++j){
          futures_pu[i][j] = _sp->idot(p[i],u[j]);
        }
      }


      // zr
      for(size_t i=0;i<steps;++i){
        alpha[i] = futures_zr[i].get();
      }
      // pu
      for(size_t i=0; i<steps;++i){
        for(size_t j=0; j<steps;++j){
          pu[i][j] = futures_pu[i][j].get();
        }
      }
      // todo: update beta
      beta = pq_inv;
      beta.rightmultiply(pu);
      // zu
      for(size_t i=0; i<steps;++i){
        for(size_t j=0; j<steps;++j){
          pq_inv[i][j] = futures_zu[i][j].get();
        }
      }
      pu.transpose();
      pu.rightmultiply(beta);
      pq_inv.add(-1.0,pu);
      pq_inv.invert();
      pq_inv.mv(alpha);
    }

  protected:
    unsigned int steps = 2;
    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_reduction;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("sstepcgsolver", defaultIterativeSolverCreator<Dune::SStepCGSolver>());
}

#endif
