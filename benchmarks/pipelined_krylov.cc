// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/matrixredistribute.hh>
#include <dune/istl/schwarz.hh>
#include <dune/istl/test/laplacian.hh>
#include <dune/istl/pipelinedcg.hh>

using namespace Dune;

template<template<class> class Solver, class X>
void test(const Dune::ParameterTree& config,
          const X& x_orig,
          const X& b_orig,
          std::shared_ptr<Dune::LinearOperator<X,X>> op,
          std::shared_ptr<Dune::ScalarProduct<X>> sp,
          std::shared_ptr<Dune::Preconditioner<X,X>> prec){
  Dune::Timer watch;
  int maxit = config.get("maxit", 1000);
  int verbose = config.get("verbose", 1);
  double reduction = config.get("reduction", 1e-5);
  Dune::InverseOperatorResult res;
  X x(x_orig);
  X b(b_orig);
  Solver<X> solver0(op, sp, prec, reduction,maxit,verbose);
  if(solver0.name() == config.get("solver", solver0.name())){
    solver0.apply(x,b, res);
  }
}

int main(int argc, char** argv){
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  Dune::ParameterTree config;
  Dune::ParameterTreeParser::readOptions(argc, argv, config);
  try{
    Dune::ParameterTreeParser::readINITree(config.get("config", "config.ini"), config, false);
  }catch(...){}

  int N=config.get("N", 30);

  int rank = mpihelper.rank();
  int verbose = config.get("verbose",1);
  verbose = (config.get("verbose_rank", 0) == rank)?verbose:0;
  config["verbose"] = std::to_string(verbose);

  typedef int GlobalId;
  typedef Dune::OwnerOverlapCopyCommunication<GlobalId> Communication;
  typedef Dune::RedistributeInformation<Communication> RedistInfo;
  typedef Dune::BCRSMatrix<double> BCRSMat;
  typedef typename Dune::Amg::MatrixGraph<BCRSMat> MatrixGraph;
  typedef Dune::BlockVector<double> BVector;
  typedef Dune::OverlappingSchwarzOperator<BCRSMat, BVector, BVector, Communication> Operator;
  typedef Dune::OverlappingSchwarzScalarProduct<BVector, Communication> ScalarProduct;

  BCRSMat A_local, A;

  auto world = mpihelper.getCommunication();
  Communication oocomm_local(world);
  std::shared_ptr<Communication> pOocomm = std::make_shared<Communication>(world);

  if(world.rank()==0)
    setupLaplacian(A_local, N);
  BVector b_local(A_local.N()), x_local(A_local.M()), x, b;
  if(world.rank()==0){
    b_local = 1.0;
    setBoundary(x_local,b_local,N);
  }

  if(world.size() > 1){
    oocomm_local.remoteIndices().template rebuild<false>();
    MatrixGraph graph(A_local);
    RedistInfo ri;
    Dune::graphRepartition(graph, oocomm_local, world.size(),
                           pOocomm, ri.getInterface());
    redistributeMatrix(A_local,A, oocomm_local, *pOocomm,
                       ri);
    x.resize(A.M());
    b.resize(A.N());
    ri.redistribute(x_local, x);
    ri.redistribute(b_local, b);
  }else{
    std::swap(A,A_local);
    std::swap(x,x_local);
    std::swap(b,b_local);
  }
  pOocomm->remoteIndices().template rebuild<false>();

  std::shared_ptr<Operator> fop = std::make_shared<Operator>(A, *pOocomm);
  std::shared_ptr<ScalarProduct> sp = std::make_shared<ScalarProduct>(*pOocomm);
  std::shared_ptr<Dune::Preconditioner<BVector,BVector>> prec0
    = std::make_shared<Dune::ParSSOR<BCRSMat,BVector,BVector, Communication>>(A, 1,1.0, *pOocomm);

  for(int r=0; r < config.get("repeat", 1); ++r){
    test<Dune::CGSolver, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::TwoReductionCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::GroppCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::OneReductionCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::PartiallyPipelinedCG, BVector>(config, x, b, fop, sp, prec0);
    test<Dune::GhyselsPipelinedCG, BVector>(config, x, b, fop, sp, prec0);
  }
  return 0;
}
