// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/blockproducts.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/schwarz.hh>

#include <dune/istl/test/laplacian.hh>
#include <dune/istl/paamg/test/anisotropic.hh>

#define MAX_VECTOR_SIZE 512

#include <dune/vectorclass/vectorclass.hh>

using namespace Dune;

template<class X>
void test(ScalarProduct<X> sp){
  using namespace Dune::Simd;
  std::cout << "testing " << Dune::className(sp) << std::endl;
  constexpr size_t N = Dune::Simd::lanes<typename X::field_type>();
  typedef Scalar<typename X::field_type> scalar_field_type;
  X x(100), y(100);
  x = 10;
  y = 2;

  auto n = sp.inorm(x);
  auto norm = n.get();
  Future<Block<X>> f = sp.idot(x,y);
  Block<X> b = f.get();

  scalar_field_type frobenius_norm = 0.0;
  for(size_t i=0; i<lanes(norm); ++i){
    frobenius_norm += lane(i, norm)*lane(i,norm);
  }
  frobenius_norm = std::sqrt(frobenius_norm);

  // check if the squared frobenius norm is the trace of the block inner product
  b = sp.idot(x,x).get();
  scalar_field_type trace = 0.0;
  for(size_t l=0; l<N; ++l){
    trace += lane(l, b.diagonal());
  }
  if(std::abs(frobenius_norm - std::sqrt(trace)) > 1e-7)
    DUNE_THROW(Exception, "The frobenius norm is not the square root of the trace!");

  // test normalize
  Block<X> r = sp.inormalize(x).get();
  Block<X> qtq = sp.idot(x,x).get();
  FieldMatrix<scalar_field_type, N, N> qtq_m(qtq);
  for(size_t i=0;i<N;++i){
    qtq_m[i][i] -= 1.0;
  }
  if(qtq_m.frobenius_norm() > 1e-7)
    DUNE_THROW(Exception, "Normalize failed!");

  // check frobeniusnorm of r
  Dune::FieldMatrix<scalar_field_type, N, N> r_mat(r);
  if(std::abs(frobenius_norm-r_mat.frobenius_norm()) > 1e-7)
    DUNE_THROW(Exception, "R factor has wrong frobenius norm!");

  X z(x);
  z = 0.0;
  r.axpy(1.0, z, x);
  for(size_t i=0;i<z.size();++i){
    for(size_t l=0;l<N;++l){
      if(std::abs(10-Dune::Simd::lane(l,z[i])) > 1e-7)
        DUNE_THROW(Exception, "Not a QR decomposition!");
    }
  }
}

template<class X>
void fillRandom(X& x){
  for(size_t i=0;i<x.size();++i){
    for(size_t l=0;l<Simd::lanes(x[i]);++l){
      lane(l, x[i]) = drand48();
    }
  }
}

int main(int argc, char** argv){
  auto& mpihelper = MPIHelper::instance(argc, argv);
  auto world = mpihelper.getCommunication();
  {
    using X = BlockVector<double>;
    test(SequentialBlockProduct<X, 1, false>{});
    test(SequentialBlockProduct<X, 1, true>{});
  }

  {
    using X = BlockVector<LoopSIMD<Vec8d, 2>>;
    test(SequentialBlockProduct<X, 1, false>{});
    test(SequentialBlockProduct<X, 1, true>{});
    test(SequentialBlockProduct<X, 2, false>{});
    test(SequentialBlockProduct<X, 2, true>{});
    test(SequentialBlockProduct<X, 8, false>{});
    test(SequentialBlockProduct<X, 8, true>{});
    test(SequentialBlockProduct<X, 16, false>{});
    test(SequentialBlockProduct<X, 16, true>{});
  }

  {
    using X = BlockVector<LoopSIMD<double, 8>>;
    test(SequentialBlockProduct<X, 1, false>{});
    test(SequentialBlockProduct<X, 1, true>{});
    test(SequentialBlockProduct<X, 2, false>{});
    test(SequentialBlockProduct<X, 2, true>{});
    test(SequentialBlockProduct<X, 8, false>{});
    test(SequentialBlockProduct<X, 8, true>{});
  }

  {
    using X = BlockVector<LoopSIMD<double, 4>>;
    using Comm = OwnerOverlapCopyCommunication<int>;
    Comm comm(world);
    ParallelBlockProduct<X, Comm, 4> bp(comm, SolverCategory::overlapping);
    test(bp);
  }

  {
    using X = BlockVector<LoopSIMD<Vec8d, 2>>;
    using Comm = OwnerOverlapCopyCommunication<int>;
    Comm comm(world);
    ParallelBlockProduct<X, Comm, 4> bp(comm, SolverCategory::overlapping);
    test(bp);
  }

  {
    using X = BlockVector<LoopSIMD<Vec8d, 2>>;
    using Comm = OwnerOverlapCopyCommunication<int>;
    Comm comm(world);
    ParallelBlockProduct<X, Comm, 4, true> bp(comm, SolverCategory::overlapping);
    test(bp);
  }

  // if(0){
  //   using X = BlockVector<LoopSIMD<double, 4>>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   ParallelBlockProduct<X, Comm, 1> bp(comm, SolverCategory::overlapping);
  //   test(bp);
  // }
  // if(0){
  //   using X = BlockVector<LoopSIMD<double, 4>>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   ParallelBlockProduct<X, Comm, 2> bp(comm, SolverCategory::overlapping);
  //   test(bp);
  // }
  // {
  //   using X = BlockVector<LoopSIMD<double, 4>>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   ParallelBlockProduct<X, Comm, 4> bp(comm, SolverCategory::overlapping);
  //   test(bp);
  // }

  // {
  //   using X = BlockVector<LoopSIMD<double, 64>>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   ParallelBlockProduct<X, Comm, 32> bp(comm, SolverCategory::overlapping);
  //   test(bp);
  // }

  // {
  //   using X = BlockVector<LoopSIMD<double, 8>>;
  //   using Mat = BCRSMatrix<double>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   int N = 100, n;
  //   Mat A = setupAnisotropic2d<double>(N, comm.indexSet(), world, &n);
  //   comm.remoteIndices().template rebuild<false>();
  //   X x(A.M()), b(A.N());
  //   //fillRandom(b);
  //   b = 1.0;
  //   x = 0.0;
  //   setBoundary(x,b, n, comm.indexSet());

  //   ParallelScalarProduct<X, Comm> bp(comm, SolverCategory::overlapping);
  //   OverlappingSchwarzOperator<Mat, X, X, Comm> op(A, comm);
  //   ParSSOR<Mat, X, X, Comm> prec(A, 1.0, 1, comm);
  //   CGSolver<X> solver(op, bp, prec, 1e-8, 200, world.rank()==0?2:0);
  //   InverseOperatorResult res;
  //   solver.apply(x,b, res);
  // }

  // {
  //   using X = BlockVector<LoopSIMD<double, 4>>;
  //   using Mat = BCRSMatrix<double>;
  //   using Comm = OwnerOverlapCopyCommunication<int>;
  //   Comm comm(world);
  //   int N = 100, n;
  //   Mat A = setupAnisotropic2d<double>(N, comm.indexSet(), world, &n);
  //   comm.remoteIndices().template rebuild<false>();
  //   X x(A.M()), b0(A.N());
  //   fillRandom(b0);
  //   //b0 = 1;
  //   x = 0.0;
  //   setBoundary(x,b0, n, comm.indexSet());
  //   X b(b0);
  //   OverlappingSchwarzOperator<Mat, X, X, Comm> op(A, comm);
  //   ParallelBlockProduct<X, Comm, 4> bp(comm, SolverCategory::overlapping);
  //   op.applyscaleadd(-1, x, b);
  //   auto initial_defect = bp.norm(b0);
  //   std::cout << "initial defect " << initial_defect << std::endl;
  //   b = b0;

  //   ParSSOR<Mat, X, X, Comm> prec(A, 1.0, 1, comm);
  //   double reduction = 1e-8;
  //   CGSolver<X> solver(op, bp, prec, reduction, 100, world.rank()==0?2:0);
  //   InverseOperatorResult res;
  //   solver.apply(x,b, res);
  //   op.applyscaleadd(-1, x, b0);
  //   auto defect = bp.norm(b0);
  //   std::cout << "solver defect " << defect << std::endl;
  //   if(Dune::Simd::allTrue(defect > reduction*initial_defect)){
  //     std::cout << "real reduction is " << defect/initial_defect << std::endl;
  //     DUNE_THROW(Exception, "Solver did not reach the desired reduction!");
  //   }
  // }
  return 0;
}
