// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_BLOCKSSOR_HH
#define DUNE_ISTL_BLOCKSSOR_HH

#include "preconditioner.hh"

#include <tbb/tbb.h>

namespace Dune {

  template<class Matrix, class X, class Y>
  class BlockSSOR : public Preconditioner<X,Y> {
  public:
    typedef Matrix matrix_type;

    typedef X domain_type;
    typedef Y range_type;

    typedef typename X::field_type field_type;
    typedef Simd::Scalar<field_type> scalar_field_type;

    BlockSSOR (std::shared_ptr<const Matrix> A, scalar_field_type w, size_t blocks)
      : _A(A)
      , _w(w)
      , _blocks(blocks)
    {}

    BlockSSOR (const Matrix& A, scalar_field_type w, size_t blocks = 1)
      : _A(stackobject_to_shared_ptr(A))
      , _w(w)
      , _blocks(blocks)
    {}

    BlockSSOR (const std::shared_ptr<const AssembledLinearOperator<Matrix,X,Y>>& A,
               const ParameterTree config)
      : _A(stackobject_to_shared_ptr(A->getmat()))
      , _w(config.get("w",1.0))
      , _blocks(config.get("blocks", 1))
    {}

    virtual void pre(X& x, Y& b){
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
    }

    virtual void apply(X& x, const Y& b){
      size_t n = x.size();
      tbb::task_group g;
      for(size_t block=0;block<_blocks;++block){
        size_t first = (block*n)/_blocks;
        size_t last = ((block+1)*n)/_blocks;
        g.run([first,last,&x,&b,&A=*_A,w=this->_w]{
                // backward
                typedef typename Matrix::block_type block_type;
                block_type diag;
                for(size_t i=last; i>first; --i){
                  x[i-1]=b[i-1];
                  x[i-1]*=((field_type(2.0)-w)/w);
                  for(auto col_it=A[i-1].begin(); col_it!=A[i-1].end();++col_it){
                    auto index = col_it.index();
                    if(index >i-1 && index < last){
                      auto&& u = Impl::asVector(x[i-1]);
                      Impl::asMatrix(*col_it).mmv(Impl::asVector(x[index]), u);
                    }
                    if(index == i-1){
                      diag = *col_it;
                    }
                  }
                  x[i-1] *= w;
                  auto tmp = x[i-1];
                  auto&& u = Impl::asVector(x[i-1]);
                  Impl::asMatrix(diag).solve(u, Impl::asVector(tmp));
                }
                // scale diagonal
                for(size_t i=first; i<last;++i){
                  auto tmp = x[i];
                  auto&& u = Impl::asVector(x[i]);
                  Impl::asMatrix(A[i][i]).mv(Impl::asVector(tmp), u);
                }
                // forward
                for(size_t i=first; i<last; ++i){
                  for(auto col_it=A[i].begin(); col_it!=A[i].end();++col_it){
                    auto index = col_it.index();
                    if(index < i && index >= first){
                      auto&& u = Impl::asVector(x[i]);
                      Impl::asMatrix(*col_it).mmv(Impl::asVector(x[index]), u);
                    }
                    if(index == i){
                      diag = *col_it;
                    }
                  }
                  x[i] *= w;
                  auto tmp = x[i];
                  auto&& u = Impl::asVector(x[i]);
                  Impl::asMatrix(diag).solve(u, Impl::asVector(tmp));
                }
              });
      }
      g.wait();
    }

    virtual void post(X& x){
      DUNE_UNUSED_PARAMETER(x);
    }

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  protected:
    std::shared_ptr<const Matrix> _A;
    scalar_field_type _w;
    size_t _blocks;
  };
  DUNE_REGISTER_PRECONDITIONER("blockssor", defaultPreconditionerCreator<Dune::BlockSSOR>());
}

#endif
