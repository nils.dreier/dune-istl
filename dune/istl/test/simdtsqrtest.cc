// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include "config.h"

#include <dune/common/simd/loop.hh>
#include <dune/istl/simdtsqr.hh>
#include <dune/istl/bvector.hh>

template<typename T>
struct Random {
  static T gen()
  {
    return drand48();
  }
};

template<class T, int N>
struct Random<Dune::FieldVector<T,N>>{
  static Dune::FieldVector<T,N> gen(){
    Dune::FieldVector<T,N> result;
    for(size_t i = 0; i<N;++i){
      result[i] = Random<T>::gen();
    }
    return result;
  }
};

template<class T, size_t N>
struct Random<Dune::LoopSIMD<T,N>>{
  static Dune::LoopSIMD<T,N> gen(){
    Dune::LoopSIMD<T,N> result;
    for(size_t i = 0; i<N;++i){
      Dune::Simd::lane(i,result) = Random<T>::gen();
    }
    return result;
  }
};

template<class B>
void test(){
  typedef Dune::BlockVector<B> V;

  constexpr size_t N = 100;
  V v(N);
  for(size_t i=0;i<N;++i){
    v[i] = Random<B>::gen();
  }
  auto r = Dune::Lapack::tsqr(v,true, std::integral_constant<size_t, Dune::Simd::lanes<B>()>{});
  std::cout << r << std::endl;
}

int main(int argc, char** argv){

  test<double>();
  test<Dune::LoopSIMD<double,4>>();
  test<Dune::FieldVector<Dune::LoopSIMD<double,4>,2>>();
  return 0;
}
