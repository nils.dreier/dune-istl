// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_SIMDTSQR_HH
#define DUNE_ISTL_SIMDTSQR_HH

#include <type_traits>

#include <dune/common/simd/simd.hh>
#include <dune/istl/bvector.hh>

#define SGEQRF_FORTRAN FC_FUNC (sgeqrf, SGEQRF)
#define SORGQR_FORTRAN FC_FUNC (sorgqr, SORGQR)
#define DGEQRF_FORTRAN FC_FUNC (dgeqrf, DGEQRF)
#define DORGQR_FORTRAN FC_FUNC (dorgqr, DORGQR)
#define CGEQRF_FORTRAN FC_FUNC (cgeqrf, CGEQRF)
#define CUNGQR_FORTRAN FC_FUNC (cungqr, CUNGQR)
#define ZGEQRF_FORTRAN FC_FUNC (zgeqrf, ZGEQRF)
#define ZUNGQR_FORTRAN FC_FUNC (zungqr, ZUNGQR)

// dsyev declaration (in liblapack)
extern "C" {
  typedef const long int integer;
  int DGEQRF_FORTRAN(integer *m, integer *n, double *a,
                     integer *lda, double *tau, double *work,
                     integer *lwork, integer *info);

  int DORGQR_FORTRAN(integer *m, integer *n, integer *k, double *a,
                     integer *lda, double *tau, double *work, integer *lwork,
                     integer *info);

  int SGEQRF_FORTRAN(integer *m, integer *n, float *a,
                     integer *lda, float *tau, float *work,
                     integer *lwork, integer *info);

  int SORGQR_FORTRAN(integer *m, integer *n, integer *k, float *a,
                     integer *lda, float *tau, float *work, integer *lwork,
                     integer *info);

  int CGEQRF_FORTRAN(integer *m, integer *n, std::complex<float> *a,
                     integer *lda, std::complex<float> *tau, std::complex<float> *work,
                     integer *lwork, integer *info);

  int CUNGQR_FORTRAN(integer *m, integer *n, integer *k, std::complex<float> *a,
                     integer *lda, std::complex<float> *tau, std::complex<float> *work, integer *lwork,
                     integer *info);

  int ZGEQRF_FORTRAN(integer *m, integer *n, std::complex<double> *a,
                     integer *lda, std::complex<double> *tau, std::complex<double> *work,
                     integer *lwork, integer *info);

  int ZUNGQR_FORTRAN(integer *m, integer *n, integer *k, std::complex<double> *a,
                     integer *lda, std::complex<double> *tau, std::complex<double> *work, integer *lwork,
                     integer *info);
}

namespace Dune{
  namespace Lapack{
    using namespace Simd;

    namespace Impl{
      void xGEQRF(integer *m, integer *n, float* a, integer *lda, float* tau, float* work, integer* lwork, integer* info){
        SGEQRF_FORTRAN(m,n,a,lda,tau,work,lwork,info);
      }
      void xGEQRF(integer *m, integer *n, double* a, integer *lda, double* tau, double* work, integer* lwork, integer* info){
        DGEQRF_FORTRAN(m,n,a,lda,tau,work,lwork,info);
      }
      void xGEQRF(integer *m, integer *n, std::complex<float>* a, integer *lda, std::complex<float>* tau, std::complex<float>* work, integer* lwork, integer* info){
        CGEQRF_FORTRAN(m,n,a,lda,tau,work,lwork,info);
      }
      void xGEQRF(integer *m, integer *n, std::complex<double>* a, integer *lda, std::complex<double>* tau, std::complex<double>* work, integer* lwork, integer* info){
        ZGEQRF_FORTRAN(m,n,a,lda,tau,work,lwork,info);
      }

      void xORGQR(integer *m, integer *n, integer *k, float *a,
                  integer *lda, float *tau, float *work, integer *lwork,
                  integer *info){
        SORGQR_FORTRAN(m,n,k,a,lda,tau,work,lwork,info);
      }
      void xORGQR(integer *m, integer *n, integer *k, double *a,
                  integer *lda, double *tau, double *work, integer *lwork,
                  integer *info){
        DORGQR_FORTRAN(m,n,k,a,lda,tau,work,lwork,info);
      }
      void xORGQR(integer *m, integer *n, integer *k, std::complex<float> *a,
                  integer *lda, std::complex<float> *tau, std::complex<float> *work, integer *lwork,
                  integer *info){
        CUNGQR_FORTRAN(m,n,k,a,lda,tau,work,lwork,info);
      }
      void xORGQR(integer *m, integer *n, integer *k, std::complex<double> *a,
                  integer *lda, std::complex<double> *tau, std::complex<double> *work, integer *lwork,
                  integer *info){
        ZUNGQR_FORTRAN(m,n,k,a,lda,tau,work,lwork,info);
      }
    }

    template<class B, size_t P>
    FieldMatrix<Scalar<typename B::field_type>, lanes<typename B::field_type>(),lanes<typename B::field_type>()>
    tsqr(B& a, bool computeQ = false, std::integral_constant<size_t, P> = {}, bool global = false){
      using std::sqrt;
      typedef Scalar<typename B::field_type> scalar_field_type;
      constexpr size_t N = lanes<typename B::field_type>();
      assert(N%P==0);
      constexpr size_t Q = N/P;
      FieldMatrix<scalar_field_type,N,N> r(0.0);
      if(!global){
        for(size_t q=0; q<Q;++q){
          integer m = a.dim();
          integer n = P;
          std::vector<scalar_field_type> a_buf(n*m);
          for(size_t i=0;i<P;++i){
            for(size_t j=0, k=0;j<a.size();++j){
              auto&& aj = Dune::Impl::asVector(a[j]);
              for(size_t l=0;l<aj.size();++l,++k){
                a_buf[i*m+k] = Simd::lane(i+q*P, aj[l]);
              }
            }
          }
          std::vector<scalar_field_type> tau(n);
          integer lwork = n;
          integer info = 0;
          std::vector<scalar_field_type> work(lwork);
          Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);
          if(info != 0)
            DUNE_THROW(Exception, "LAPACK DGEQRF failed!");

          // now the upper triangle of a contains R
          for(size_t i=0;i<P;++i){
            for(size_t j=i;j<P;++j){
              r[i+q*P][j+q*P] = a_buf[j*m+i];
            }
          }

          // compute q
          if (computeQ){
            std::fill(work.begin(), work.end(), 0.0);
            Impl::xORGQR(&m,&n,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);
            if(info != 0)
              DUNE_THROW(Exception, "LAPACK DORGQR failed!");
          }

          // copy back to BlockVector
          for(size_t i=0;i<P;++i){
            for(size_t j=0,k=0;j<a.size();++j){
              auto&& aj = Dune::Impl::asVector(a[j]);
              for(size_t l=0;l<aj.size();++l,++k){
                Simd::lane(i+q*P,aj[l]) = a_buf[i*m+k];
              }
            }
          }
        }
      }else{
        integer m = a.dim()*Q;
        integer n = P;
        std::vector<scalar_field_type> a_buf(n*m);
        for(size_t q=0; q<Q; ++q){
          for(size_t i=0;i<P;++i){
            for(size_t j=0, k=0;j<a.size();++j){
              auto&& aj = Dune::Impl::asVector(a[j]);
              for(size_t l=0;l<aj.size();++l,++k){
                a_buf[q*a.dim() + i*m+k] = Simd::lane(i+q*P, aj[l]);
              }
            }
          }
        }
        std::vector<scalar_field_type> tau(n);
        integer lwork = n;
        integer info = 0;
        std::vector<scalar_field_type> work(lwork);
        Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);
        if(info != 0)
          DUNE_THROW(Exception, "LAPACK DGEQRF failed!");

        // now the upper triangle of a contains R
        for(size_t i=0;i<P;++i){
          for(size_t j=i;j<P;++j){
            for(size_t q=0; q<Q;++q){
              r[i+q*P][j+q*P] = 1.0/sqrt(Q) * a_buf[j*m+i];
            }
          }
        }

        // compute q
        if (computeQ){
          std::fill(work.begin(), work.end(), 0.0);
          Impl::xORGQR(&m,&n,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);
          if(info != 0)
            DUNE_THROW(Exception, "LAPACK DORGQR failed!");
        }

        // copy back to BlockVector
        for(size_t q=0;q<Q;++q){
          for(size_t i=0;i<P;++i){
            for(size_t j=0,k=0;j<a.size();++j){
              auto&& aj = Dune::Impl::asVector(a[j]);
              for(size_t l=0;l<aj.size();++l,++k){
                Simd::lane(i+q*P,aj[l]) = sqrt(Q) * a_buf[q*a.dim() + i*m+k];
              }
            }
          }
        }
      }
      return r;
    }

    template<class FT, int K>
    static FieldMatrix<FT,K,K> tsqr_reduce(std::vector<FieldMatrix<FT,K,K>>& as){
      size_t len = as.size();
      integer m = len*K;
      integer n = K;
      std::vector<FT> a_buf(len*K*K); // fortran numbering!
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          for(size_t k=0;k<len;++k){
            a_buf[j*len*K+i+k*K] = as[k][i][j];
          }
        }
      }
      std::vector<FT> tau(K);
      integer lwork = n;
      integer info = 0;
      std::vector<FT> work(lwork);
      Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);

      FieldMatrix<FT,K,K> r(0.0);
      // now the upper triangle of a contains R
      for(size_t i=0;i<K;++i){
        for(size_t j=i;j<K;++j){
          r[i][j] = a_buf[j*m+i];
        }
      }

      // compute q
      Impl::xORGQR(&m,&n,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);

      // copy back to BlockVector
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          for(size_t k=0;k<len;++k){
            as[k][i][j] = a_buf[j*len*K+i+k*K];
          }
        }
      }

      return r;
    }

    template<class Block>
    static Block tsqr_reduce(std::vector<Block>& as){
      Block r = as[0];
      using X=typename Block::Vector;
      using field_type = typename X::field_type;
      constexpr size_t K = Simd::lanes<field_type>();
      using scalar_field_type = Simd::Scalar<field_type>;
      using FM = Dune::FieldMatrix<scalar_field_type, K, K>;
      std::vector<FM> fms (as.size());
      for(size_t i=0;i<as.size();++i){
        fms[i] = FM(as[i]);
      }
      r = tsqr_reduce(fms);
      for(size_t i=0;i<as.size();++i){
        as[i] = fms[i];
      }
      return r;
    }

    template<class Block>
    static Block tsqr_reduce(Block& a, Block& b){
      Block r = a;
      using X=typename Block::Vector;
      using field_type = typename X::field_type;
      constexpr size_t K = Simd::lanes<field_type>();
      using scalar_field_type = Simd::Scalar<field_type>;
      using FM = Dune::FieldMatrix<scalar_field_type, K, K>;
      std::vector<FM> fms = {a,b};
      r = tsqr_reduce(fms);
      a = fms[0];
      b = fms[1];
      return r;
    }

    template<class Block>
    static Block tsqr_reduce(Block* a, Block* b, int count){
      using X=typename Block::Vector;
      using field_type = typename X::field_type;
      using scalar_field_type = Simd::Scalar<field_type>;
      constexpr size_t K = Simd::lanes<field_type>();
      integer m = 2*count*K;
      integer n = K;
      std::vector<scalar_field_type> a_buf(2*count*K*K); // fortran numbering!
      for(size_t k=0;k<count;++k){
        FieldMatrix<scalar_field_type, K, K> am = FieldMatrix<scalar_field_type, K, K>(a[k]);
        FieldMatrix<scalar_field_type, K, K> bm = FieldMatrix<scalar_field_type, K, K>(b[k]);
        for(size_t i=0;i<K;++i){
          for(size_t j=0;j<K;++j){
            a_buf[j*2*count*K+i+k*K] = am[i][j];
            a_buf[j*2*count*K+i+(k+count)*K] = bm[i][j];
          }
        }
      }
      std::vector<scalar_field_type> tau(K);
      integer lwork = n;
      integer info = 0;
      std::vector<scalar_field_type> work(lwork);
      Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);

      FieldMatrix<scalar_field_type,K,K> r(0.0);
      // now the upper triangle of a contains R
      for(size_t i=0;i<K;++i){
        for(size_t j=i;j<K;++j){
          r[i][j] = a_buf[j*m+i];
        }
      }

      // compute q
      Impl::xORGQR(&m,&n,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);

      // copy back to BlockVector
      for(size_t k=0;k<count;++k){
        FieldMatrix<scalar_field_type, K, K> am, bm;
        for(size_t i=0;i<K;++i){
          for(size_t j=0;j<K;++j){
            am[i][j] = a_buf[j*2*count*K+i+k*K];
            bm[i][j] = a_buf[j*2*count*K+i+(k+count)*K];
          }
        }
        a[k] = am;
        b[k] = bm;
      }

      Block result;
      result = r;
      return result;
    }
  }

}

#endif
