#include <config.h>
#include <iostream>
#include <numeric>

#include <dune/common/alignedallocator.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parametertree.hh>

#include <dune/common/simd/loop.hh>
#if HAVE_DUNE_VECTORCLASS
#include <dune/vectorclass/vecarray.hh>
#endif
#if HAVE_VC
#include <dune/common/simd/vc.hh>
#endif

#if HAVE_STDSIMD
#include <dune/common/simd/stdsimd.hh>
#endif

#include <dune/istl/bvector.hh>
#include <dune/istl/blockproducts.hh>

#include "benchmark.hh"

using namespace Dune;
using namespace Dune::Simd;

#if NDEBUG
constexpr size_t N = 6000000; // 6M doubles = 48MB; ensure to be out of L3-Cache
#else
constexpr size_t N = 6000;
#endif

template<class SIMD, size_t P, bool GLOBAL = false>
struct BaxpyBenchmark{
  static constexpr size_t K = lanes<SIMD>();
  static constexpr bool execute = K%P==0;
  typedef Dune::BlockVector<SIMD, Dune::AlignedAllocator<SIMD>> Vector;
  SequentialBlockProduct<Vector,P, GLOBAL> sp;
  Vector x, y;
  Block<Vector> mat;

  BaxpyBenchmark(const ParameterTree& config)
    : x(config.get("N", N))
    , y(config.get("N", N)){
    std::iota(x.begin(), x.end(), -1.2);
    std::fill(y.begin(), y.end(), -1.5);
    mat = sp.idot(x,y).get();
  }

  ~BaxpyBenchmark(){
    asm volatile ("" :: "m" (y));
  }

  void run(){
    mat.axpy(-1.0, y, x);
  }

  size_t flops() const{
    return 2*K*P*x.size();
  }

  std::string name() const{
    std::string simd = className<SIMD>();
    size_t pos = simd.find("LoopSIMD");
    if(pos != std::string::npos){
      size_t pos2 = simd.find(",",pos);
      simd = "LoopSIMD(" + simd.substr(pos+9, pos2-pos-9) + ")";
    }
    return "BAXPYbenchmark("+simd+")";
  }

  static void printHeader(){
    std::cout << std::setw(8) << "P"
              << std::setw(8) << "Q"
              << std::setw(8) << "GLOBAL"
              << std::setw(16) << "N";
  }

  void printArgs() const{
    std::cout << std::setw(8) << P
              << std::setw(8) << K/P
              << std::setw(8) << (GLOBAL?1:0)
              << std::setw(16) << x.size();
  }
};

template<size_t P>
using Benchmark_VecArray = BaxpyBenchmark<VecArray<double,P>, P>;

template<size_t P>
using Benchmark_LoopSIMD = BaxpyBenchmark<LoopSIMD<double,P,std::min(64ul,sizeof(double)*P)>, P>;

typedef std::index_sequence<1,2,4,8,16,24,32,40,48,56,64,72,80,96,128,192,256> Ps;

constexpr size_t FixK = 256;

template<size_t P>
using Benchmark_VecArray_fixK = BaxpyBenchmark<VecArray<double,FixK, FixK/P>, P>;

template<size_t P>
using Benchmark_LoopSIMD_fixK = BaxpyBenchmark<LoopSIMD<double,FixK,std::min(64ul,sizeof(double)*P)>, P>;

template<size_t P>
using Benchmark_VecArray_fixK_GLOBAL = BaxpyBenchmark<VecArray<double,FixK, FixK/P>, P, true>;

template<size_t P>
using Benchmark_LoopSIMD_fixK_GLOBAL = BaxpyBenchmark<LoopSIMD<double,FixK,std::min(64ul,sizeof(double)*P)>, P, true>;


int main(int argc, char** argv){
  ParameterTree config;
  ParameterTreeParser::readOptions(argc, argv, config);
  initTBB(config.get("workers", tbb::task_scheduler_init::default_num_threads()));
  std::string simd = config.get("simd", "VectorClass");
  bool isFixK = config.get("fixK", false);
  bool isGLOBAL = config.get("global", false);
  if(isFixK){
    if(simd == "VectorClass" && isGLOBAL){
      benchmark<Benchmark_VecArray_fixK_GLOBAL>(config, Ps{});
    }else if(simd == "LoopSIMD" && isGLOBAL){
      benchmark<Benchmark_LoopSIMD_fixK_GLOBAL>(config, Ps{});
    }else if(simd == "VectorClass"){
      benchmark<Benchmark_VecArray_fixK>(config, Ps{});
    }else if(simd == "LoopSIMD"){
      benchmark<Benchmark_LoopSIMD_fixK>(config, Ps{});
    }else{
      std::cerr << "Unknown SIMD" << std::endl;
      return 1;
    }
  }else{
    if(simd == "VectorClass"){
      benchmark<Benchmark_VecArray>(config, Ps{});
    }else if(simd == "LoopSIMD"){
      benchmark<Benchmark_LoopSIMD>(config, Ps{});
    }else{
      std::cerr << "Unknown SIMD" << std::endl;
      return 1;
    }
  }
  return 0;
}
