#!/bin/bash

spin_time=5
workers=2

./bdotbenchmark -spin_time ${spin_time} -workers 1 | tee bdot_seq.dat
./bdotbenchmark -spin_time ${spin_time} -workers ${workers}| tee bdot_para.dat

./baxpybenchmark -spin_time ${spin_time} -workers 1 | tee baxpy_seq.dat
./baxpybenchmark -spin_time ${spin_time} -workers ${workers} | tee baxpy_para.dat

./bdotbenchmark -spin_time ${spin_time} -workers 1 -fixK 1 -N 100000 | tee bdot_seq_fixK.dat
./bdotbenchmark -spin_time ${spin_time} -workers ${workers} -fixK 1 -N 100000 | tee bdot_para_fixK.dat

./baxpybenchmark -spin_time ${spin_time} -workers 1 -fixK 1  -N 100000 | tee baxpy_seq_fixK.dat
./baxpybenchmark -spin_time ${spin_time} -workers ${workers} -fixK 1  -N 100000 | tee baxpy_para_fixK.dat

./bdotbenchmark -spin_time ${spin_time} -workers 1 -fixK 1 -global 1 -N 100000 | tee bdot_seq_fixK_GLOBAL.dat
./bdotbenchmark -spin_time ${spin_time} -workers ${workers} -fixK 1 -global 1 -N 100000 | tee bdot_para_fixK_GLOBAL.dat

./baxpybenchmark -spin_time ${spin_time} -workers 1 -fixK 1  -N 100000 | tee baxpy_seq_fixK.dat
./baxpybenchmark -spin_time ${spin_time} -workers ${workers} -fixK 1  -N 100000 | tee baxpy_para_fixK.dat

./bopbenchmark -spin_time ${spin_time} -workers 1 -pattern 2DFD | tee bop_2DFD_seq.dat
./bopbenchmark -spin_time ${spin_time} -workers ${workers} -pattern 2DFD| tee bop_2DFD_para.dat

./bopbenchmark -spin_time ${spin_time} -workers 1 -pattern 3DQ1 | tee bop_3DQ1_seq.dat
./bopbenchmark -spin_time ${spin_time} -workers ${workers} -pattern 3DQ1 | tee bop_3DQ1_para.dat
