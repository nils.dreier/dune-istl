// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>

#include <tbb/tbb.h>

typedef double field_type;

typedef Dune::BCRSMatrix<Dune::FieldMatrix<Dune::Simd::Scalar<field_type>,1,1>> Mat;
typedef Dune::BlockVector<Dune::FieldVector<field_type,1>> Vec;

int main(int argc, char** argv){
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  Dune::ParameterTree config;
  Dune::ParameterTreeParser::readOptions(argc, argv, config);

  int workers = config.get("workers", tbb::task_scheduler_init::default_num_threads());
  std::cout << "using " << workers << " workers" << std::endl;
  tbb::task_scheduler_init init(workers);

  std::string matrixfilename = config.get<std::string>("matrix");
  Mat m;
  loadMatrixMarket(m, matrixfilename);
  Vec x(m.N()), y(x.N());

  Dune::Timer t;
  for(int rep=0; rep<=config.get("rep", 10); ++rep)
    m.mv(x, y);
  std::cout << t.elapsed() << std::endl;
  return 0;
}
