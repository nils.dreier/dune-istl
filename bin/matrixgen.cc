#include <config.h>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/test/laplacian.hh>

typedef Dune::BCRSMatrix<double> Matrix;

int main(int argc, char** argv){
  Dune::ParameterTree config;
  Dune::ParameterTreeParser::readOptions(argc, argv, config);

  Matrix A;
  int N = config.get("N", 1000);
  setupLaplacian(A, N);

  std::ostringstream rfilename;
  rfilename << "laplacian_" << N << ".mm";
  Dune::storeMatrixMarket(A, rfilename.str());
  return 0;
}
