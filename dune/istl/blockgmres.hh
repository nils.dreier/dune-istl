// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_BLOCKGMRES_HH
#define DUNE_ISTL_BLOCKGMRES_HH

#include <dune/istl/scalarproducts.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solverfactory.hh>
#include <dune/istl/simdtsqr.hh>

namespace Dune {
  template<class X>
  class BlockGMRES : public IterativeSolver<X, X>
  {
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type=Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();
    typedef FieldMatrix<scalar_field_type, K, K> FieldMatrixBlock;

    BlockGMRES(const std::shared_ptr<LinearOperator<X,X>>& op,
               const std::shared_ptr<ScalarProduct<X>>& sp,
               const std::shared_ptr<Preconditioner<X,X>>& prec,
               const ParameterTree& config = {})
      : IterativeSolver<X,X>(op,sp,prec, config)
      , _restart(config.get("restart", 20))
      , _right_preconditioning(config.get("right_preconditioning", true))
    {}

    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      X b0(b);
      _op->applyscaleadd(-1.0,x,b);

      // Krylov basis
      std::vector<X> v(_restart+1, b);
      if(_right_preconditioning){
        v[0] = b;
      }else{
        v[0] = 0.0;
        _prec->apply(v[0], b);
      }

      // orthonormalize v[0]
      // transformed residual
      std::vector<Block<X>> s(_restart+1);
      s[0] = _sp->inormalize(v[0]).get();

      real_type norm = frobenius_norms(s[0]);
      if(iteration.step(0, norm)){
        _prec->post(x);
        return;
      }

      Block<X> block0 = s[0];
      block0 = FieldMatrixBlock(0.0);
      // transformations
      std::vector<std::array<Block<X>, 4>> Q(_restart, {block0,block0,block0,block0});
      // H matrix
      std::vector<std::vector<Block<X>>> H(_restart, std::vector<Block<X>>(_restart,block0));

      X tmp(v[0]);

      int j = 1;
      while(j<= _maxit && res.converged != true){
        int i = 0;
        for(i=0; i < _restart && !res.converged; ++j){
          // block arnoldi
          if(_right_preconditioning){
            tmp = 0.0;
            _prec->apply(tmp, v[i]);
            _op->apply(tmp, v[i+1]);
          }else{
            _op->apply(v[i], tmp);
            v[i+1] = 0.0;
            _prec->apply(v[i+1], tmp);
          }
          for(int k=0;k<i+1; ++k){
            H[k][i] = _sp->idot(v[k],v[i+1]).get();
            H[k][i].axpy(-1.0, v[i+1], v[k]);
          }
          Block<X> normalizer = _sp->inormalize(v[i+1]).get();
          // apply QR transformations
          for(int k=0; k<i;++k){
            applyTransformation(Q[k], H[k][i], H[k+1][i]);
          }
          // compute new QR transformation
          H[i][i] = computeTransform(FieldMatrixBlock(H[i][i]), FieldMatrixBlock(normalizer), Q[i][0], Q[i][1], Q[i][2], Q[i][3]);
          // apply new qr transformation on residual
          s[i+1] = block0;
          applyTransformation(Q[i], s[i], s[i+1]);

          i++;
          real_type norm = frobenius_norms(s[i]);
          if(iteration.step(j, norm))
            break;
        }
        blockBackSubstitute(H,s,i);

        for(size_t k=0;k<i;++k){
          s[k].axpy(1.0, x, v[k]);
        }
        if(!res.converged && j < _maxit ){
          if(_verbose > 0)
            std::cout << "=== BlockGMRes::restart" << std::endl;
          b = b0;
          if(!_right_preconditioning){
            _op->applyscaleadd(-1.0, x, b);
            v[0] = 0.0;
            _prec->apply(v[0], b);
          }else{
            tmp = 0.0;
            _prec->apply(tmp, x);
            v[0] = b;
            _op->applyscaleadd(-1.0, tmp, v[0]);
          }
          s[0] = _sp->inormalize(v[0]).get();
        }
      }
      if(_right_preconditioning){
        tmp = x;
        x = 0.0;
        _prec->apply(x, tmp);
      }
      _prec->post(x);
    }

  protected:
    real_type frobenius_norms(const Block<X>& x){
      FieldMatrixBlock m(x);
      real_type result = 0.0;
      for(size_t i=0;i<Simd::lanes(result); ++i){
        lane(i,result) = m[i].two_norm();
      }
      return result;
    }

    void blockBackSubstitute(std::vector<std::vector<Block<X>>>& H,
                             std::vector<Block<X>>& b,
                             size_t l){
      for(int i=l-1;i>=0; --i){
        b[i].scale(-1.0);
        for(size_t j=i+1;j<l;++j){
          H[i][j].rightmultiply(b[j]);
          b[i].add(H[i][j]);
        }
        b[i].scale(-1.0);
        H[i][i].invert();
        b[i].leftmultiply(H[i][i]);
      }
    }

    void applyTransformation(const std::array<Block<X>, 4>& t,
                             Block<X>& h1, Block<X>& h2){
      Block<X> tmp1 = h1, tmp2 = h2;
      h1.leftmultiply(t[0]);
      tmp2.leftmultiply(t[2]);
      h1.add(tmp2);
      h2.leftmultiply(t[3]);
      tmp1.leftmultiply(t[1]);
      h2.add(tmp1);
    }

    FieldMatrixBlock computeTransform(const FieldMatrixBlock& h1, const FieldMatrixBlock& h2,
                          Block<X>& q11, Block<X>& q12, Block<X>& q21, Block<X>& q22){
      // use LAPACK to compute the complete QR decomposition of [h1,h2]^T
      integer m = 2*K;
      integer n = K;
      // allocate already for the output
      std::vector<scalar_field_type> a_buf(m*m); // fortran numbering!
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          a_buf[j*2*K+i] = h1[i][j];
          a_buf[j*2*K+i+K] = h2[i][j];
        }
      }
      std::vector<scalar_field_type> tau(n);
      integer lwork = m;
      integer info = 0;
      std::vector<scalar_field_type> work(lwork);
      Lapack::Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);
      if(info != 0)
        DUNE_THROW(Exception, "LAPACK DGEQRF failed!");

      FieldMatrixBlock result(0.0);
      for(size_t i=0;i<K;++i){
        for(size_t j=i;j<K;++j){
          result[i][j] = a_buf[j*m+i];
        }
      }
      Lapack::Impl::xORGQR(&m,&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);
      if(info != 0)
        DUNE_THROW(Exception, "LAPACK DORGQR failed!");
      //scatter a_buf to the output matrices (and transpose)
      FieldMatrixBlock p11,p12,p21,p22;
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          p11[j][i] = a_buf[j*2*K +i];
          p12[j][i] = a_buf[j*2*K + i + 2*K*K];
          p21[j][i] = a_buf[j*2*K +i + K];
          p22[j][i] = a_buf[j*2*K +i + K + 2*K*K];
        }
      }

      q11 = p11;
      q12 = p12;
      q21 = p21;
      q22 = p22;
      return result;
    }

    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
    int _restart = 20;
    bool _right_preconditioning = true;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("blockgmres", defaultIterativeSolverCreator<Dune::BlockGMRES>());



  template<class X>
  class HierarchicBlockGMRES : public IterativeSolver<X, X>
  {
  public:
    using typename IterativeSolver<X,X>::domain_type;
    using typename IterativeSolver<X,X>::range_type;
    using typename IterativeSolver<X,X>::field_type;
    using typename IterativeSolver<X,X>::real_type;
    using typename IterativeSolver<X,X>::scalar_real_type;
    using scalar_field_type=Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();
    typedef FieldMatrix<scalar_field_type, K, K> FieldMatrixBlock;

    HierarchicBlockGMRES(const std::shared_ptr<LinearOperator<X,X>>& op,
               const std::shared_ptr<ScalarProduct<X>>& sp,
               const std::shared_ptr<Preconditioner<X,X>>& prec,
               const ParameterTree& config = {})
      : IterativeSolver<X,X>(op,sp,prec, config)
      , _restart(config.get("restart", 20))
      , _right_preconditioning(config.get("right_preconditioning", true))
    {}

    // don't shadow four-argument version of apply defined in the base class
    using IterativeSolver<X,X>::apply;

    virtual void apply (X& x, X& b, InverseOperatorResult& res)
    {
      Iteration iteration(*this,res);
      _prec->pre(x,b);             // prepare preconditioner

      X r(b);
      _op->applyscaleadd(-1.0,x,r);

      // Krylov basis
      std::vector<X> v(_restart+1, r);
      if(_right_preconditioning){
        v[0] = r;
      }else{
        v[0] = 0.0;
        _prec->apply(v[0], r);
      }

      // residual transformations
      std::vector<Block<X>> s(_restart+1);
      std::shared_ptr<GramSchmidt<X>> gs = _sp->gramSchmidt();
      s[0] = gs->orthonormalize(v[0],0,v).get()[0];
      Block<X> block0 = s[0];
      block0 = FieldMatrixBlock(0.0);

      real_type norm = frobenius_norms(s[0]);
      if(iteration.step(0, norm)){
        _prec->post(x);
        return;
      }

      // transformations
      std::vector<std::array<Block<X>, 4>> Q(_restart, {block0,block0,block0,block0});
      // H matrix
      std::vector<std::vector<Block<X>>> H(_restart);
      // temporary vector for application of the preconditioner
      X tmp(v[0]);

      int j = 1;
      while(j<= _maxit && res.converged != true){
        int i = 0;
        for(i=0; i < _restart && !res.converged; ++j){
          // get global last v
          gs->getGlobal(v, i, v[i+1]);
          // block arnoldi
          if(_right_preconditioning){
            tmp = 0.0;
            _prec->apply(tmp, v[i+1]);
            _op->apply(tmp, v[i+1]);
          }else{
            _op->apply(v[i+1], tmp);
            v[i+1] = 0.0;
            _prec->apply(v[i+1], tmp);
          }
          H[i] = gs->orthonormalize(v[i+1], i+1, v);
          // apply QR transformations
          for(int k=0; k<i;++k){
            applyTransformation(Q[k], H[i][k], H[i][k+1]);
          }
          // compute new QR transformation
          H[i][i] = computeTransform(FieldMatrixBlock(H[i][i]), FieldMatrixBlock(H[i][i+1]), Q[i][0], Q[i][1], Q[i][2], Q[i][3]);
          H[i][i+1] = block0;
          // apply new qr transformation on residual
          s[i+1] = block0;
          applyTransformation(Q[i], s[i], s[i+1]);

          i++;
          norm = frobenius_norms(s[i]);
          if(iteration.step(j, norm))
            break;
        }
        blockBackSubstitute(H,s,i);

        for(size_t k=0;k<i;++k){
          gs->getGlobal(v, k, tmp);
          s[k].axpy(1.0, x, tmp);
        }
        gs->reset();
        if(!res.converged && j < _maxit ){
          if(_verbose > 0)
            std::cout << "=== BlockGMRes::restart" << std::endl;
          if(!_right_preconditioning){
            r = b;
            _op->applyscaleadd(-1.0, x, r);
            v[0] = 0.0;
            _prec->apply(v[0], r);
          }else{
            tmp = 0.0;
            _prec->apply(tmp, x);
            v[0] = b;
            _op->applyscaleadd(-1.0, tmp, v[0]);
          }
          s[0] = gs->orthonormalize(v[0], 0, v).get()[0];
          // norm = frobenius_norms(s[0]);
          // iteration.step(j-1, norm);
        }
      }
      if(_right_preconditioning){
        tmp = x;
        x = 0.0;
        _prec->apply(x, tmp);
      }
      _prec->post(x);
    }

  protected:
    real_type frobenius_norms(const Block<X>& x){
      FieldMatrixBlock m(x);
      real_type result = 0.0;
      for(size_t i=0;i<Simd::lanes(result); ++i){
        lane(i,result) = m[i].two_norm();
      }
      return result;
    }

    void blockBackSubstitute(std::vector<std::vector<Block<X>>>& H,
                             std::vector<Block<X>>& b,
                             size_t l){
      for(int i=l-1;i>=0; --i){
        b[i].scale(-1.0);
        for(size_t j=i+1;j<l;++j){
          H[j][i].rightmultiply(b[j]);
          b[i].add(H[j][i]);
        }
        b[i].scale(-1.0);
        H[i][i].invert();
        b[i].leftmultiply(H[i][i]);
      }
    }

    void applyTransformation(const std::array<Block<X>, 4>& t,
                             Block<X>& h1, Block<X>& h2){
      Block<X> tmp1 = h1, tmp2 = h2;
      h1.leftmultiply(t[0]);
      tmp2.leftmultiply(t[2]);
      h1.add(tmp2);
      h2.leftmultiply(t[3]);
      tmp1.leftmultiply(t[1]);
      h2.add(tmp1);
    }

    FieldMatrixBlock computeTransform(const FieldMatrixBlock& h1, const FieldMatrixBlock& h2,
                          Block<X>& q11, Block<X>& q12, Block<X>& q21, Block<X>& q22){
      // use LAPACK to compute the complete QR decomposition of [h1,h2]^T
      integer m = 2*K;
      integer n = K;
      // allocate already for the output
      std::vector<scalar_field_type> a_buf(m*m); // fortran numbering!
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          a_buf[j*2*K+i] = h1[i][j];
          a_buf[j*2*K+i+K] = h2[i][j];
        }
      }
      std::vector<scalar_field_type> tau(n);
      integer lwork = m;
      integer info = 0;
      std::vector<scalar_field_type> work(lwork);
      Lapack::Impl::xGEQRF(&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork, &info);
      if(info != 0)
        DUNE_THROW(Exception, "LAPACK DGEQRF failed!");

      FieldMatrixBlock result(0.0);
      for(size_t i=0;i<K;++i){
        for(size_t j=i;j<K;++j){
          result[i][j] = a_buf[j*m+i];
        }
      }
      Lapack::Impl::xORGQR(&m,&m,&n,a_buf.data(),&m,tau.data(),work.data(),&lwork,&info);
      if(info != 0)
        DUNE_THROW(Exception, "LAPACK DORGQR failed!");
      //scatter a_buf to the output matrices (and transpose)
      FieldMatrixBlock p11,p12,p21,p22;
      for(size_t i=0;i<K;++i){
        for(size_t j=0;j<K;++j){
          p11[j][i] = a_buf[j*2*K +i];
          p12[j][i] = a_buf[j*2*K + i + 2*K*K];
          p21[j][i] = a_buf[j*2*K +i + K];
          p22[j][i] = a_buf[j*2*K +i + K + 2*K*K];
        }
      }

      q11 = p11;
      q12 = p12;
      q21 = p21;
      q22 = p22;
      return result;
    }

    using IterativeSolver<X,X>::_op;
    using IterativeSolver<X,X>::_prec;
    using IterativeSolver<X,X>::_sp;
    using IterativeSolver<X,X>::_maxit;
    using IterativeSolver<X,X>::_verbose;
    using Iteration = typename IterativeSolver<X,X>::template Iteration<unsigned int>;
    int _restart = 20;
    bool _right_preconditioning = true;
  };
  DUNE_REGISTER_ITERATIVE_SOLVER("hierarchicblockgmres", defaultIterativeSolverCreator<Dune::HierarchicBlockGMRES>());
}

#endif
