// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixmarket.hh>

#include <tbb/tbb.h>

typedef double field_type;

typedef Dune::BlockVector<Dune::FieldVector<field_type,1>> Vec;

int main(int argc, char** argv){
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  Dune::ParameterTree config;
  Dune::ParameterTreeParser::readOptions(argc, argv, config);

  int workers = config.get("workers", tbb::task_scheduler_init::default_num_threads());
  std::cout << "using " << workers << " workers" << std::endl;
  tbb::task_scheduler_init init(workers);

  bool weak_scaling = config.get("weak_scaling", 0);
  const size_t n = config.get("N", 10000)*(weak_scaling?workers:1);
  std::cout << "n=" << n << std::endl;
  Vec x(n), y(n);

  //axpy
  std::cout << "axpy:\t" << std::flush;
  Dune::Timer t;
  for(int rep=0; rep<=config.get("rep", 10); ++rep)
    x.axpy(-1.0, y);
  std::cout << t.elapsed() << std::endl;
  return 0;
}
