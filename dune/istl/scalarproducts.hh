// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_SCALARPRODUCTS_HH
#define DUNE_ISTL_SCALARPRODUCTS_HH

#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/future.hh>
#include <dune/common/typeerasure.hh>
#include <dune/common/dynmatrix.hh>
#include <dune/common/parametertree.hh>
#include <dune/istl/simdtsqr.hh>

#include "bvector.hh"
#include "solvercategory.hh"
#include "tsqr_communication_pattern/tsqr_pattern.hh"

namespace Dune {
  /**
   * @defgroup ISTL_SP Scalar products
   * @ingroup ISTL_Solvers
   * @brief Scalar products for the use in iterative solvers
   */
  /** @addtogroup ISTL_SP
          @{
   */

  /** \file

     \brief Define base class for scalar product and norm.

     These classes have to be implemented differently for different
     data partitioning strategies. Default implementations for the
     sequential case are provided.

   */

  namespace Imp {
    template<class X>
    class BlockProductTraits {
    public:
      class alignas(typename X::field_type) Concept{
        using field_type = typename X::field_type;
        using scalar_field_type = Simd::Scalar<field_type>;
        static constexpr size_t K = Simd::lanes<field_type>();
      public:
        virtual ~Concept() {}
        virtual void axpy(const scalar_field_type& alpha, X& x, const X& y) const = 0;
        virtual void invert() = 0;
        virtual void transpose() = 0;
        virtual Concept& add(const Concept& other) = 0;
        virtual Concept& scale(const scalar_field_type& factor) = 0;
        virtual Concept& leftmultiply(const Concept& other) = 0;
        virtual Concept& rightmultiply(const Concept& other) = 0;
        virtual field_type diagonal() const = 0;
        virtual explicit operator FieldMatrix<scalar_field_type,K,K> () const = 0;
        virtual Concept& operator=(const FieldMatrix<scalar_field_type,K,K>&) = 0;
        virtual Concept& operator=(const scalar_field_type&) = 0;
      };

      template<class Impl>
      class Model : public Impl{
        using field_type = typename X::field_type;
        using scalar_field_type = Simd::Scalar<field_type>;
        using Wrapped = typename Impl::Wrapped;
        static constexpr size_t K = Simd::lanes<field_type>();
      public:
        using Impl::Impl;
        virtual void axpy(const scalar_field_type& alpha, X& x, const X& y) const override {
          this->getWrapped().axpy(alpha, x, y);
        }
        virtual void invert() override {
          this->getWrapped().invert();
        }
        virtual void transpose() override {
          this->getWrapped().transpose();
        }
        virtual Model& add(const Concept& other) override {
          this->getWrapped().add(dynamic_cast<const Model&>(other).getWrapped());
          return *this;
        }
        virtual Model& scale(const scalar_field_type& factor) override {
          this->getWrapped().scale(factor);
          return *this;
        }
        virtual Model& leftmultiply(const Concept& other) override {
          this->getWrapped().leftmultiply(dynamic_cast<const Model&>(other).getWrapped());
          return *this;
        }
        virtual Model& rightmultiply(const Concept& other) override {
          this->getWrapped().rightmultiply(dynamic_cast<const Model&>(other).getWrapped());
          return *this;
        }
        virtual field_type diagonal() const override{
          return this->getWrapped().diagonal();
        }
        virtual explicit operator FieldMatrix<scalar_field_type,K,K> () const override{
          return this->getWrapped().operator FieldMatrix<scalar_field_type,K,K>();
        }
        virtual Model& operator=(const FieldMatrix<scalar_field_type,K,K>& o) override{
          this->getWrapped() = o;
          return *this;
        }
        virtual Model& operator=(const scalar_field_type& o) override{
          this->getWrapped() = o;
          return *this;
        }
      };
    };
  } // namespace Imp

  template<class X>
  class Block :
  public TypeErasureBase<
    typename Imp::template BlockProductTraits<X>::Concept,
    Imp::template BlockProductTraits<X>::template Model,
    64
    >{
    using Base = TypeErasureBase<
      typename Imp::template BlockProductTraits<X>::Concept,
      Imp::template BlockProductTraits<X>::template Model,
      64>;

    using field_type = typename X::field_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    static constexpr size_t K = Simd::lanes<field_type>();
  public:
    template<class P, disableCopyMove<Block, P> = 0 >
    Block(P&& p)
      : Base(std::forward<P>(p))
    {}

    Block() = default;

    void axpy(const scalar_field_type& alpha, X& x, const X& y) const{
      this->asInterface().axpy(alpha, x, y);
    }

    Block& invert(){
      this->asInterface().invert();
      return *this;
    }

    Block& transpose(){
      this->asInterface().transpose();
      return *this;
    }

    Block& add(const Block& other){
      this->asInterface().add(other.asInterface());
      return *this;
    }

    Block& scale(const scalar_field_type& factor){
      this->asInterface().scale(factor);
      return *this;
    }

    Block& leftmultiply(const Block& other){
      this->asInterface().leftmultiply(other.asInterface());
      return *this;
    }

    Block& rightmultiply(const Block& other){
      this->asInterface().rightmultiply(other.asInterface());
      return *this;
    }

    field_type diagonal() const{
      return this->asInterface().diagonal();
    }

    explicit operator FieldMatrix<scalar_field_type,K,K>() const {
      return this->asInterface().operator FieldMatrix<scalar_field_type,K,K>();
    }

    Block& operator=(const FieldMatrix<scalar_field_type,K,K>& o) {
      this->asInterface() = o;
      return *this;
    }

    Block& operator=(const scalar_field_type& o) {
      this->asInterface() = o;
      return *this;
    }

    friend std::ostream& operator << (std::ostream& s, const Block& b)
    {
      using field_type = typename X::field_type;
      using scalar_field_type = Simd::Scalar<field_type>;
      constexpr size_t k = Simd::lanes<field_type>();
      if(!(bool)b.wrapped_)
        s << "Block not initialized!";
      else
      s << FieldMatrix<scalar_field_type, k, k>(b);
      return s;
    }
  };

  template<class X>
  class ScalarProductResult{
    typedef typename FieldTraits<X>::field_type field_type;
    typedef typename FieldTraits<X>::real_type real_type;
    using scalar_field_type = Simd::Scalar<field_type>;
    field_type _data;
    static constexpr size_t K = Simd::lanes<field_type>();
  public:
    using Vector=X;

    ScalarProductResult(const field_type& d = 0.0)
      : _data(d)
    {}
    void axpy(const scalar_field_type& alpha, X& x, const X& y) const {
      x.axpy(alpha*_data, y);
    }
    void invert() {
      _data = real_type(1.0)/_data;
    }
    void transpose() {}
    ScalarProductResult& add(const ScalarProductResult& other){
      _data += other._data;
      return *this;
    }
    ScalarProductResult& scale(const scalar_field_type& factor){
      _data *= factor;
      return *this;
    }
    ScalarProductResult& leftmultiply(const ScalarProductResult& other){
      _data *= other._data;
      return *this;
    }
    ScalarProductResult& rightmultiply(const ScalarProductResult& other){
      _data *= other._data;
      return *this;
    }
    field_type diagonal() const{
      return _data;
    }
    explicit operator FieldMatrix<scalar_field_type,K,K>() const{
      using namespace Dune::Simd;
      FieldMatrix<scalar_field_type,K,K> result = 0.0;
      for(size_t i=0;i<K; ++i){
        result[i][i] = lane(i, _data);
      }
      return result;
    }
    ScalarProductResult& operator=(const FieldMatrix<scalar_field_type,K,K>& o) {
      using namespace Dune::Simd;
      for(size_t i=0;i<K;++i){
        lane(i,_data) = o[i][i];
      }
      return *this;
    }

    ScalarProductResult& operator=(const scalar_field_type& o) {
      using namespace Dune::Simd;
      _data = o;
      return *this;
    }
  };

  template<class X>
  class ScalarProduct;

  template<class X>
  class GramSchmidt{
  public:
    virtual ~GramSchmidt(){}
    virtual Future<std::vector<Block<X>>> orthonormalize(X& x, size_t i, const std::vector<X>& v) = 0;
    virtual void reset() = 0;
    virtual void getGlobal(const std::vector<X>& v, size_t i, X& x) const = 0;
  };

  template<class X>
  class ModifiedGramSchmidt : public GramSchmidt<X>{
  public:
    ModifiedGramSchmidt(const ScalarProduct<X>& sp)
      : _sp(sp)
    {}

    virtual Future<std::vector<Block<X>>>
    orthonormalize(X& x, size_t i, const std::vector<X>& v){
      std::vector<Block<X>> result(i+1);
      for(size_t j=0;j<i;++j){
        result[j] = _sp.idot(v[j], x).get();
        result[j].axpy(-1.0, x, v[j]);
      }
      result[i] = _sp.inormalize(x).get();
      return PseudoFuture<std::vector<Block<X>>>(std::move(result));
    }

    virtual void getGlobal(const std::vector<X>& v, size_t i, X& x) const override {
      x = v[i];
    }

    virtual void reset(){}

  protected:
    const ScalarProduct<X>& _sp;
  };

  template<class X>
  class PipelinedHybridGramSchmidt : public GramSchmidt<X>{
  public:
    PipelinedHybridGramSchmidt(const ScalarProduct<X>& sp, size_t r = 1, size_t it = 1)
      : _sp(sp)
      , _r(r)
      , _it(it)
    {}

    virtual Future<std::vector<Block<X>>>
    orthonormalize(X& x, size_t i, const std::vector<X>& v){
      std::vector<Block<X>> result(i+1);
      std::queue<Future<Block<X>>> futures;
      size_t r = std::min<size_t>(i,_r);
      for(size_t it = 0; it < _it; ++it){
        for(size_t j=0;j<r;++j){
          futures.push(_sp.idot(v[j], x));
        }
        for(size_t j=r;j<i;++j){
          futures.push(_sp.idot(v[j], x));
          auto tmp = futures.front().get();
          tmp.axpy(-1.0, x, v[j-r]);
          if(it==0)
            result[j-r] = tmp;
          else
            result[j-r].add(tmp);
          futures.pop();
        }
        for(size_t j=i-r;j<i;++j){
          auto tmp = futures.front().get();
          tmp.axpy(-1.0, x, v[j]);
          if(it==0)
            result[j] = tmp;
          else
            result[j].add(tmp);
          futures.pop();
        }
      }
      result[i] = _sp.inormalize(x).get();
      return PseudoFuture<std::vector<Block<X>>>(std::move(result));
    }

    virtual void getGlobal(const std::vector<X>& v, size_t i, X& x) const override {
      x = v[i];
    }

    virtual void reset(){}

  protected:
    const ScalarProduct<X>& _sp;
    size_t _r, _it;
  };

  template<class X, class C, class BlockType>
  class DistributedGramSchmidt : public GramSchmidt<X>{
    typedef typename X::field_type field_type;
    typedef typename X::block_type block_type;
    public:
    DistributedGramSchmidt(std::function<BlockType(const X&, const X&)> local_dot,
                          std::function<BlockType(X&)> local_normalize,
                          const C& comm)
      : _local_dot(local_dot)
      , _local_normalize(local_normalize)
      , _comm(comm)
      , tree(TSQR_REDUCE::binaryReductionTree(_comm))
      , ops(tree.depth())
    {}

    class Ops {
    public:

      void reduce(BlockType* a, BlockType* b, int count, BlockType* out){
        // orthorgonalize [a,b] against previous blocks
        for( size_t j=0; j<_localBasis.size();++j){
          BlockType tmp;
          out[j] = Simd::Scalar<field_type>(0.0);
          size_t rs2 = _localBasis[j].size()/2;
          for(size_t i=0; i<rs2;++i){
            tmp = _localBasis[j][i];
            tmp.transpose();
            tmp.rightmultiply(a[i]);
            out[j].add(tmp);
            tmp = _localBasis[j][i+rs2];
            tmp.transpose();
            tmp.rightmultiply(b[i]);
            out[j].add(tmp);
          }
          for(size_t i=0; i<rs2; ++i){
            tmp = out[j];
            tmp.leftmultiply(_localBasis[j][i]);
            tmp.scale(-1.0);
            a[i].add(tmp);
            tmp = out[j];
            tmp.leftmultiply(_localBasis[j][i+rs2]);
            tmp.scale(-1.0);
            b[i].add(tmp);
          }
        }
        out[count-1] = Lapack::tsqr_reduce(a,b,count);
        _localBasis.push_back({a, a+count});
        _localBasis.back().insert(_localBasis.back().end(), b, b+count);
      }

      void backpropagate(BlockType* a, int count, const BlockType* r, bool upper){
        for(size_t i=0;i<count;++i)
          a[i] = 0.0;
        for( size_t k = 0; k<_localBasis.size(); ++k ){
          for(size_t i=0; i<k+1;++i){
            BlockType tmp = _localBasis[k][i+(upper?0:(k+1))];
            tmp.rightmultiply(r[k]);
            a[i].add(tmp);
          }
        }
      }

    public:
      void clear(){
        _localBasis.clear();
      }

      std::vector<std::vector<BlockType>> _localBasis;
    };

    virtual Future<std::vector<Block<X>>>
    orthonormalize(X& x, size_t i, const std::vector<X>& v){
      std::vector<BlockType> result(i+1);
      for(size_t j=0;j<i;++j){
        result[j] = _local_dot(v[j], x);
        result[j].axpy(-1.0, x, v[j]);
      }
      result[i] = _local_normalize(x);

      _local_transforms.push_back(std::vector<BlockType>(i+1));
      int c = 0;
      int d = 0;
      TSQR_REDUCE::tsqr_pattern(_local_transforms.back().data(),
                                result.data(),
                                i+1,
                                MPITraits<BlockType>::getType(),
                                [&](BlockType* a, BlockType* b, int count, BlockType* out){
                                  ops[c].reduce(a,b,count,out);
                                  ++c;
                                },
                                [&](BlockType* a, int count, const BlockType* r){
                                  if(a != _local_transforms.back().data()){
                                    ops[tree.depth() - d/2 - (tree.has_root()?2:1)].backpropagate(a, count, r, d%2==0);
                                    ++d;
                                  }else{
                                    for(size_t i=0;i<count;++i){
                                      a[i] = r[i];
                                    }
                                  }
                                },
                                _comm,
                                tree);

      std::vector<Block<X>> re(i+1);
      for(size_t j=0;j<i+1; ++j){
        re[j] = Block<X>(result[j]);
      }
      return PseudoFuture<std::vector<Block<X>>>(std::move(re));
    }

    virtual void getGlobal(const std::vector<X>& v, size_t i, X& x) const override{
      if(_comm.size() == 1){
        x = v[i];
        return;
      }
      x = 0.0;
      for(size_t j=0;j<i+1;++j){
        _local_transforms[i][j].axpy(1.0, x, v[j]);
      }
    }

    virtual void reset() {
      _local_transforms.clear();
      for( auto& op : ops)
        op.clear();
    }

  protected:
    std::vector<std::vector<BlockType>> _local_transforms;

    std::function<BlockType(const X&, const X&)> _local_dot;
    std::function<BlockType(X&)> _local_normalize;
    const C& _comm;
    TSQR_REDUCE::ReductionTree tree;
    std::vector<Ops> ops;
  };

  /*! \brief Base class for scalar product and norm computation

      Krylov space methods need to compute scalar products and norms
      (for convergence test only). These methods have to know about the
          underlying data decomposition. For the sequential case a default implementation
          is provided.

      by default the scalar product is sequential
   */
  template<class X>
  class ScalarProduct {
  public:
    //! export types, they come from the derived class
    typedef X domain_type;
    typedef typename X::field_type field_type;
    typedef typename FieldTraits<field_type>::real_type real_type;

    /*! \brief Dot product of two vectors.
       It is assumed that the vectors are consistent on the interior+border
       partition.
     */
    virtual field_type dot (const X& x, const X& y) const
    {
      return x.dot(y);
    }

    virtual Future<Block<X>> idot(const X& x, const X& y) const{
      return PseudoFuture<Block<X>>(ScalarProductResult<X>(x.dot(y)));
    }

    /*! \brief Norm of a right-hand side vector.
       The vector must be consistent on the interior+border partition
     */
    virtual real_type norm (const X& x) const
    {
      return inorm(x).get();
    }

    virtual Future<real_type> inorm(const X& x) const{
      return PseudoFuture<real_type>(x.two_norm());
    }

    // returns a Block alpha, s.t. x = y alpha^T, with orthogonal y
    // When the future is complete, y is stored in x
    virtual Future<Block<X>> inormalize(X& x) const{
      real_type n = norm(x);
      x /= n;
      return PseudoFuture<Block<X>>(ScalarProductResult<X>(n));
    }

    virtual std::shared_ptr<GramSchmidt<X>> gramSchmidt() const {
      return std::make_shared<ModifiedGramSchmidt<X>>(*this);
    }

    //! Category of the scalar product (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

    //! every abstract base class has a virtual destructor
    virtual ~ScalarProduct () {}
  };

  /**
   * \brief Scalar product for overlapping Schwarz methods.
   *
   * Consistent vectors in interior and border are assumed.
   * \tparam  X The type of the sequential vector to use for the left hand side,
   * e.g. BlockVector or another type fulfilling the ISTL
   * vector interface.
   * \tparam C The type of the communication object.
   * This must either be OwnerOverlapCopyCommunication or a type
   * implementing the same interface.
   */
  template<class X, class C>
  class ParallelScalarProduct : public ScalarProduct<X>
  {
  public:
    typedef typename X::field_type field_type;
  public:
    //! \brief The type of the vector to compute the scalar product on.
    //!
    //! E.g. BlockVector or another type fulfilling the ISTL
    //! vector interface.
    typedef X domain_type;
    //!  \brief The field type used by the vector type domain_type.
    typedef typename FieldTraits<field_type>::real_type real_type;
    //! \brief The type of the communication object.
    //!
    //! This must either be OwnerOverlapCopyCommunication or a type
    //! implementing the same interface.
    typedef C communication_type;

    /*!
     * \param com The communication object for syncing overlap and copy
     * data points.
     * \param cat parallel solver category (nonoverlapping or overlapping)
     */
    ParallelScalarProduct (const communication_type& com, SolverCategory::Category cat, const ParameterTree& config = {})
      : _communication(com), _category(cat), _config(config)
    {}

    /*! \brief Dot product of two vectors.
       It is assumed that the vectors are consistent on the interior+border
       partition.
     */
    virtual field_type dot (const X& x, const X& y) const override
    {
      field_type sum = 0.0;
      _communication.partial_dot(x,y,sum); // explicitly loop and apply masking
      return _communication.communicator().sum(sum);
    }

    virtual Future<Block<X>> idot(const X& x, const X& y) const override{
      field_type sum = 0.0;
      _communication.partial_dot(x,y,sum); // explicitly loop and apply masking
      Future<field_type> f = _communication.communicator().template iallreduce<std::plus<Simd::Scalar<field_type>>>(std::move(sum));
      return PostProcessFuture<Block<X>, field_type>(std::move(f), [](field_type d){
                                                                     return Block<X>(ScalarProductResult<X>(d));});
    }

    /*! \brief Norm of a right-hand side vector.
       The vector must be consistent on the interior+border partition
     */
    virtual real_type norm (const X& x) const override
    {
      return inorm(x).get();
    }

    virtual Future<real_type> inorm(const X& x) const override{
      field_type sum = 0.0;
      _communication.partial_dot(x,x,sum); // explicitly loop and apply masking
      Future<field_type> f = _communication.communicator().template iallreduce<std::plus<Simd::Scalar<field_type>>>(std::move(sum));
      using std::sqrt;
      using std::abs;
      return PostProcessFuture<real_type, field_type>(std::move(f),
                                                      [](field_type x) {return abs(sqrt(x));});
    }

    virtual Future<Block<X>> inormalize(X& x) const override{
      auto f = inorm(x);
      return PostProcessFuture<Block<X>, real_type>(std::move(f),
                                                    [&](real_type n){
                                                      x /= n;
                                                      return ScalarProductResult<X>(n);
                                                    });
    }

    virtual std::shared_ptr<GramSchmidt<X>> gramSchmidt() const override {
      std::string gramschmidt = _config.get("gramschmidt", "modified");
      if(gramschmidt == "modified")
        return std::make_shared<ModifiedGramSchmidt<X>>(*this);
      else if(gramschmidt == "classical")
        return std::make_shared<PipelinedHybridGramSchmidt<X>>(*this, 99999);
      else if(gramschmidt == "pipelinedhybrid")
        return std::make_shared<PipelinedHybridGramSchmidt<X>>(*this, _config.get("r", 1));
      else if(gramschmidt == "distributed"){
        typedef std::decay_t<decltype(_communication.communicator())> Comm;
        return std::make_shared<DistributedGramSchmidt<X, Comm, ScalarProductResult<X>>>([&](const X& x, const X& y){
                                                                                           field_type sum = 0;
                                                                                           _communication.partial_dot(x,y,sum);
                                                                                           return ScalarProductResult<X>(sum);
                                                                                         },
          [&](X& x){
            using std::sqrt;
            field_type sum = 0;
            _communication.partial_dot(x,x,sum);
            sum = sqrt(sum);
            x /= sum;
            return ScalarProductResult<X>(sum);
          },
          _communication.communicator());
      }else{
        DUNE_THROW(Dune::Exception, "Unknown Gram-Schmidt method");
      }
    }

    //! Category of the scalar product (see SolverCategory::Category)
    virtual SolverCategory::Category category() const override
    {
      return _category;
    }

  protected:
    const communication_type& _communication;
    SolverCategory::Category _category;
    const ParameterTree _config = {};
  };

  //! Default implementation for the scalar case
  template<class X>
  class SeqScalarProduct : public ScalarProduct<X>
  {
    using ScalarProduct<X>::ScalarProduct;
  };

  /**
   * \brief Nonoverlapping Scalar Product with communication object.
   *
   * Consistent vectors in interior and border are assumed.
   */
  template<class X, class C>
  class NonoverlappingSchwarzScalarProduct : public ParallelScalarProduct<X,C>
  {
  public:
    NonoverlappingSchwarzScalarProduct (const C& comm) :
      ParallelScalarProduct<X,C>(comm,SolverCategory::nonoverlapping) {}
  };

  /**
   * \brief Scalar product for overlapping Schwarz methods.
   *
   * Consistent vectors in interior and border are assumed.
   * \tparam  X The type of the sequential vector to use for the left hand side,
   * e.g. BlockVector or another type fulfilling the ISTL
   * vector interface.
   * \tparam C The type of the communication object.
   * This must either be OwnerOverlapCopyCommunication or a type
   * implementing the same interface.
   */
  template<class X, class C>
  class OverlappingSchwarzScalarProduct : public ParallelScalarProduct<X,C>
  {
  public:
    OverlappingSchwarzScalarProduct (const C& comm) :
      ParallelScalarProduct<X,C>(comm,SolverCategory::overlapping) {}
  };

  /** @} end documentation */

  /**
   * \brief Choose the approriate scalar product for a solver category.
   *
   * \todo this helper function should be replaced by a proper factory
   *
   * As there is only one scalar product for each solver category it is
   * possible to choose the appropriate product at compile time.
   *
   * In each specialization of the this struct there will be a typedef ScalarProduct
   * available the defines the type  of the scalar product.
   */
  template<class X, class Comm>
  std::shared_ptr<ScalarProduct<X>> createScalarProduct(const Comm& comm, SolverCategory::Category category)
  {
    switch(category)
    {
      case SolverCategory::sequential:
        return
          std::make_shared<ScalarProduct<X>>();
      default:
        return
          std::make_shared<ParallelScalarProduct<X,Comm>>(comm,category);
    }
  }

} // end namespace Dune

#endif
