// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_BLOCKPRODUCTS_HH
#define DUNE_ISTL_BLOCKPRODUCTS_HH

#include <set>

#include <dune/istl/scalarproducts.hh>
#include <dune/common/simd/simd.hh>
#include <dune/common/simd/loop.hh>
#include <dune/common/stdstreams.hh>
#include <dune/common/parallel/mpidata.hh>
#include <dune/istl/simdtsqr.hh>

#include "tsqr_communication_pattern/tsqr_pattern.hh"

#include <tbb/tbb.h>

namespace Dune {
  using namespace Simd;

  namespace Impl{
    // computes c += a^t b
    template<class T, size_t K>
    void mtm(const std::array<T, K>& a,
             const std::array<T, K>& b,
             std::array<T, lanes<T>()>& c){
      for(size_t i=0; i<K; ++i){
        for(size_t j=0; j<lanes<T>(); ++j){
          c[j] += lane(j, a[i])*b[i];
        }
      }
    }

    // computes c += a b
    template<class T, size_t K>
    void mm(const std::array<T, K>& a,
             const std::array<T, lanes<T>()>& b,
            std::array<T, K>& c){
      for(size_t i=0; i<lanes<T>(); ++i){
        for(size_t j=0; j<K; ++j){
          c[j] += lane(i, a[j])*b[i];
        }
      }
    }

    constexpr size_t gdle(size_t P, size_t max){
      for(size_t i=max; i>0;--i){
        if(P%i==0)
          return i;
      }
      return 1;
    }
  };

  // general impl.
  // always computes the whole k x k block and post computes the result according to P and GLOBAL
  template<class X, size_t P, bool GLOBAL, class SFINAE = void>
  class BlockProductKernel
  {
    typedef typename X::field_type field_type;
    static constexpr size_t K = lanes<field_type>();
    static_assert(K%P==0, "Only uniform blocks are allowed. I.e. P mus be a divider of K.");
    typedef Scalar<field_type> scalar_field_type;
  public:
    typedef std::array<field_type, K> data_type;
    typedef X Vector;
    data_type _data;

    BlockProductKernel(){
      std::fill(_data.begin(), _data.end(), 0.0);
    }

    void bdot(const X& x, const X& y){
      constexpr size_t tileRows = Impl::gdle(P, 4);
      tbb::blocked_range<size_t> tbbRange(0,x.size()/tileRows);
      _data = tbb::parallel_reduce(tbbRange,
                                   _data,
                                   [&](tbb::blocked_range<size_t> range, const data_type& data){
                                     data_type result(data);
                                     for(size_t r=range.begin();r<range.end();++r){
                                       size_t rk = r*tileRows;
                                       auto&& u = Impl::asVector(x[rk]);
                                       for(size_t s=0; s<u.size();++s){
                                         std::array<field_type, tileRows> a;
                                         //fill a
                                         for(size_t ia=0; ia<tileRows; ++ia)
                                           a[ia] = Impl::asVector(x[rk+ia])[s];
                                         std::array<field_type, tileRows> b;
                                         //fill b
                                         for(size_t ib=0; ib<tileRows; ++ib)
                                           b[ib] = Impl::asVector(y[rk+ib])[s];
                                         Impl::mtm(a,b,result);
                                       }
                                     }
                                     return result;
                                   },
                                   [](data_type a, const data_type& b){
                                     for(size_t i=0; i<K; ++i){
                                       a[i] += b[i];
                                     }
                                     return a;
                                   });

      for(size_t rk = (x.size()/tileRows)*tileRows; rk < x.size(); ++rk){
        auto&& u = Impl::asVector(x[rk]);
        for(size_t s=0; s<u.size();++s){
          Impl::mtm<field_type, 1>({Impl::asVector(x[rk])[s]},
                                      {Impl::asVector(y[rk])[s]},
                                      _data);
        }
      }

      if constexpr (P!=K){
        constexpr size_t Q = K/P;
        // set off-diagonal blocks to zeros
        for(size_t row=0;row<K;++row){
          for(size_t col=0; col<K;++col){
            if(row/P!=col/P)
              lane(col, _data[row]) = 0;
          }
        }
        if constexpr (GLOBAL){
          // sum up blocks
          for(size_t row=0;row<P;++row){
            for(size_t col=0;col<P;++col){
              scalar_field_type sum = 0.0;
              for(size_t q=0;q<Q;++q){
                sum += lane(q*P+col, _data[q*P+row]);
              }
              for(size_t q=0;q<Q;++q){
                lane(q*P+col, _data[q*P+row]) = sum;
              }
            }
          }
          this->scale(1.0/Q);
        }
      }
    }

    void axpy(const scalar_field_type& alpha,
              X& x, const X& y) const {
      constexpr size_t tileRows = Impl::gdle(P, 4);
      data_type scaled_mat(_data);
      for(auto& v : scaled_mat)
        v *= alpha;

      tbb::parallel_for(tbb::blocked_range<size_t>(0, x.size()/tileRows),
                        [&](tbb::blocked_range<size_t> range){
                          data_type local_cpy(scaled_mat);
                          for(size_t r = range.begin();r<range.end();++r){
                            size_t rk = r*tileRows;
                            for(size_t s=0; s<Impl::asVector(x[rk]).size();++s){
                              std::array<field_type, tileRows> a;
                              for(size_t ia=0; ia<tileRows; ++ia)
                                a[ia] = Impl::asVector(x[rk+ia])[s];
                              std::array<field_type, tileRows> b;
                              for(size_t ib=0; ib<tileRows; ++ib)
                                b[ib] = Impl::asVector(y[rk+ib])[s];
                              Impl::mm(b,local_cpy,a);
                              // copy a back to x
                              for(size_t ia=0; ia<tileRows; ++ia)
                                Impl::asVector(x[rk+ia])[s] = a[ia];
                            }
                          }
                        });
      // do the rest
      for(size_t rk = (x.size()/tileRows)*tileRows; rk < x.size(); ++rk){
        auto&& u = Impl::asVector(x[rk]);
        for(size_t s=0; s<u.size();++s){
          std::array<field_type, 1> out = {Impl::asVector(x[rk])[s]};
          Impl::mm<field_type, 1>({Impl::asVector(y[rk])[s]},
                                     scaled_mat,
                                     out);
          Impl::asVector(x[rk])[s] = out[0];
        }
      }
    }

    BlockProductKernel& invert(){
      FieldMatrix<scalar_field_type, K, K> tmp = *this;
      tmp.invert();
      *this = tmp;
      return *this;
    }

    BlockProductKernel& transpose(){
      using std::swap;
      for(size_t i=0;i<K;++i){
        for(size_t j = i+1;j<K;++j){
          swap(lane(i, _data[j]), lane(j, _data[i]));
        }
      }
      return *this;
    }

    BlockProductKernel& add(const BlockProductKernel& other){
      for(size_t i=0; i<K; ++i){
        _data[i] += other._data[i];
      }
      return *this;
    }

    BlockProductKernel& scale(const scalar_field_type& factor){
      for(auto& v: _data)
        v *= factor;
      return *this;
    }

    BlockProductKernel& leftmultiply(const BlockProductKernel& other){
      FieldMatrix<scalar_field_type, K, K> me = *this;
      FieldMatrix<scalar_field_type, K, K> you = other;
      me.leftmultiply(you);
      *this = me;
      return *this;
    }

    BlockProductKernel& rightmultiply(const BlockProductKernel& other){
      FieldMatrix<scalar_field_type, K, K> me = *this;
      FieldMatrix<scalar_field_type, K, K> you = other;
      me.rightmultiply(you);
      *this = me;
      return *this;
    }

    field_type diagonal() const{
      field_type result;
      for(size_t i = 0; i<K; ++i){
        lane(i, result) = lane(i, _data[i]);
      }
      return result;
    }

    operator FieldMatrix<scalar_field_type,K,K>() const {
      FieldMatrix<scalar_field_type, K, K> result;
      for(size_t i=0; i<K; ++i){
        for(size_t j=0; j<K;++j){
          result[i][j] = lane(j, _data[i]);
        }
      }
      return result;
    }
    BlockProductKernel& operator=(const FieldMatrix<scalar_field_type, K,K>& o){
      for(size_t i=0; i<K; ++i){
        for(size_t j=0; j<K;++j){
          lane(j, _data[i]) = o[i][j];
        }
      }
      return *this;
    }
  };

  // specialization for P=1
  template<class X, bool GLOBAL>
  class BlockProductKernel<X, 1, GLOBAL>
  {
    typedef typename X::field_type field_type;
    static constexpr size_t K = lanes<field_type>();
    typedef Scalar<field_type> scalar_field_type;
  public:
    typedef field_type data_type;
    data_type _data = 0.0;
    typedef X Vector;

    void bdot(const X& x, const X& y){
      _data = tbb::parallel_reduce(tbb::blocked_range<size_t>(0, x.size()),
                                   field_type(0.0),
                                   [&](const tbb::blocked_range<size_t>& range, const field_type& data){
                                     field_type result(data);
                                     for(size_t r=range.begin();r<range.end();++r){
                                       auto&& u=Impl::asVector(x[r]);
                                       auto&& v=Impl::asVector(y[r]);
                                       for(size_t s=0; s<u.size(); ++s){
                                         result += u[s]*v[s];
                                       }
                                     }
                                     return result;
                                   }, std::plus<field_type>());
      if constexpr(GLOBAL){
        scalar_field_type sum = 0;
        for(size_t l=0;l<K;++l)
          sum += lane(l,_data);
        _data = 1.0/K * sum;
      }
    }

    void axpy(const scalar_field_type& alpha,
              X& x, const X& y) const {
      field_type scaled = alpha*_data;

      tbb::parallel_for(tbb::blocked_range<size_t>(0,x.size()),
                        [&](const tbb::blocked_range<size_t>& range){
                          field_type local_cpy(scaled);
                          for(size_t r = range.begin();r<range.end();++r){
                            auto&& u = Impl::asVector(x[r]);
                            auto&& v = Impl::asVector(y[r]);
                            for(size_t s=0; s<u.size();++s){
                              u[s] += local_cpy*v[s];
                            }
                          }
                        });
    }

    BlockProductKernel& invert(){
      _data = field_type(1.0)/_data;
      return *this;
    }

    BlockProductKernel& transpose(){
      return *this;
    }

    BlockProductKernel& add(const BlockProductKernel& other){
      _data += other._data;
      return *this;
    }

    BlockProductKernel& scale(const scalar_field_type& factor){
      _data *= factor;
      return *this;
    }

    BlockProductKernel& leftmultiply(const BlockProductKernel& other){
      _data *= other._data;
      return *this;
    }

    BlockProductKernel& rightmultiply(const BlockProductKernel& other){
      _data *= other._data;
      return *this;
    }

    field_type diagonal() const{
      return _data;
    }

    operator FieldMatrix<scalar_field_type,K,K>() const {
      FieldMatrix<scalar_field_type, K, K> result(0.0);
      for(size_t i=0; i<K; ++i){
        result[i][i] = lane(i, _data);
      }
      return result;
    }
    BlockProductKernel& operator=(const FieldMatrix<scalar_field_type, K,K>& o){
      for(size_t i=0; i<K; ++i){
        lane(i, _data) = o[i][i];
      }
      return *this;
    }
  };

  template<class T> struct isLoopSIMD : std::false_type{};
  template<class T, size_t S, size_t A> struct isLoopSIMD<LoopSIMD<T,S,A>> : std::true_type{};

  template<class X, size_t P, bool GLOBAL>
  class BlockProductKernel<X,P, GLOBAL,
                           std::enable_if_t<isLoopSIMD<typename X::field_type>::value &&
                                            P%(lanes<std::decay_t<decltype(std::declval<typename X::field_type>()[0])>>())==0 &&
                                            lanes<typename X::field_type>()%P==0 && P!=1>>{
    using field_type = typename X::field_type;
    using scalar_field_type = Scalar<field_type>;
    static constexpr size_t K = lanes<field_type>();
    static constexpr size_t Q = K/P;
    static_assert(K%P==0, "Only uniform blocks are allowed. I.e. K%P==0");

    typedef std::decay_t<decltype(std::declval<field_type>()[0])> TileSIMD;
    static constexpr size_t tileSize = lanes<TileSIMD>();
    static constexpr size_t tiles = P/tileSize;
    static_assert(P%tileSize==0, "tileSize must be a divider of P");
#if HAVE_MPI
    friend struct MPIData<BlockProductKernel<X,P,GLOBAL>>;
#endif
  public:
    using data_type = std::array<std::array<TileSIMD, tileSize>, (GLOBAL?1:Q)*tiles*tiles>;
    data_type _data;
    typedef X Vector;

    BlockProductKernel()
    {
      for(auto& v : _data){
        for(auto& u : v){
          u = 0.0;
        }
      }
    }

    void bdot(const X& x, const X& y){
      constexpr size_t BlockTypeSize = std::decay_t<decltype(Impl::asVector(std::declval<typename X::block_type>()))>::size();

      constexpr size_t tileRows = Impl::gdle(P, 4);

      tbb::blocked_range<size_t> tbbRange(0,x.size()/tileRows);
      _data =
        tbb::parallel_reduce(tbbRange,
                             _data,
                             [&](const tbb::blocked_range<size_t>& range, const data_type& data){
                               data_type result(data);
                               for(size_t r=range.begin();r<range.end();++r){
                                 size_t rk = r*tileRows;
                                 for(size_t s=0; s<BlockTypeSize;++s){
                                   for(size_t q=0;q<Q;++q){
                                     size_t q_x_tiles = q*tiles;
                                     size_t q_offset = 0;
                                     if constexpr (!GLOBAL)
                                       q_offset = q*tiles*tiles;
                                     for(size_t i=0; i<tiles; ++i){
                                       size_t idx_i = q_x_tiles + i;
                                       std::array<TileSIMD, tileRows> a;
                                       //fill a
                                       for(size_t ia=0; ia<tileRows; ++ia)
                                         a[ia] = Impl::asVector(x[rk+ia])[s][idx_i];
                                       for(size_t j=0; j<tiles; ++j){
                                         std::array<TileSIMD, tileRows> b;
                                         //fill b
                                         size_t idx_j = q_x_tiles + j;
                                         for(size_t ib=0; ib<tileRows; ++ib)
                                           b[ib] = Impl::asVector(y[rk+ib])[s][idx_j];
                                         Impl::mtm(a,b,result[q_offset + i*tiles + j]);
                                       }
                                     }
                                   }
                                 }
                               }
                               return result;
                             }//(tbbRange, _data);
                             ,[](data_type a, const data_type& b){
                                for(size_t i=0;i<(GLOBAL?1:Q)*tiles*tiles;++i){
                                  for(size_t j=0;j<tileSize;++j){
                                    a[i][j] += b[i][j];
                                  }
                                }
                                return a;
                              });
      // do the rest
      for(size_t r=(x.size()/tileRows)*tileRows;r<x.size();++r){
        for(size_t s=0; s<Impl::asVector(x[r]).size();++s){
          for(size_t q=0;q<Q;++q){
            size_t q_offset = 0;
            if constexpr (!GLOBAL)
              q_offset = q*tiles*tiles;
            for(size_t i=0; i<tiles; ++i){
              for(size_t j=0; j<tiles; ++j){
                Impl::mtm<TileSIMD, 1>({Impl::asVector(x[r])[s][q*tiles + i]},
                                          {Impl::asVector(y[r])[s][q*tiles + j]},
                                          _data[q_offset + i*tiles + j]);
              }
            }
          }
        }
      }
      if(GLOBAL){
        this->scale(1.0/Q);
      }
    }

    void axpy(const scalar_field_type& alpha,
              X& x, const X& y) const {
      data_type scaled = _data;
      for(auto& v : scaled){
        for(auto& u : v)
          u *= alpha;
      }

      constexpr size_t tileRows = Impl::gdle(P, 4);

      constexpr size_t BlockTypeSize = std::decay_t<decltype(Impl::asVector(std::declval<typename X::block_type>()))>::size();
      tbb::blocked_range<size_t> tbbRange(0,x.size()/tileRows);
      tbb::parallel_for(tbbRange,
                        [&](const tbb::blocked_range<size_t>& range){
                          data_type local_cpy(scaled);
                          for(size_t r=range.begin();r<range.end();++r){
                            size_t rk = r*tileRows;
                            for(size_t s=0; s<BlockTypeSize;++s){
                              for(size_t q=0;q<Q;++q){
                                size_t q_x_tiles = q*tiles;
                                size_t q_offset = 0;
                                if constexpr (!GLOBAL)
                                  q_offset = q*tiles*tiles;
                                for(size_t j=0; j<tiles; ++j){
                                  std::array<TileSIMD, tileRows> b;
                                  //fill b
                                  for(size_t ib=0; ib<tileRows; ++ib)
                                    b[ib] = Impl::asVector(y[rk+ib])[s][q_x_tiles + j];
                                  for(size_t i=0; i<tiles; ++i){
                                    std::array<TileSIMD, tileRows> a;
                                    //fill a
                                    for(size_t ia=0; ia<tileRows; ++ia)
                                      a[ia] = Impl::asVector(x[rk+ia])[s][q_x_tiles + i];
                                    Impl::mm(b, local_cpy[q_offset + j*tiles + i], a);
                                    for(size_t ia=0; ia<tileRows; ++ia)
                                      Impl::asVector(x[rk+ia])[s][q_x_tiles + i] = a[ia];
                                  }
                                }
                              }
                            }
                          }
                        }
                        //(tbbRange);
                        );
      // do the rest
      for(size_t r=(x.size()/tileRows)*tileRows;r<x.size();++r){
        for(size_t s=0; s<Impl::asVector(x[r]).size();++s){
          for(size_t q=0;q<Q;++q){
            size_t q_offset = 0;
            if constexpr (!GLOBAL)
              q_offset = q*tiles*tiles;
            for(size_t i=0; i<tiles; ++i){
              for(size_t j=0; j<tiles; ++j){
                std::array<TileSIMD, 1> out = {Impl::asVector(x[r])[s][q*tiles + i]};
                Impl::mm<TileSIMD, 1>({Impl::asVector(y[r])[s][q*tiles + j]},
                                         scaled[q_offset + j*tiles + i],
                                         out);
                Impl::asVector(x[r])[s][q*tiles + i] = out[0];
              }
            }
          }
        }
      }
    }

    BlockProductKernel& invert(){
      for(size_t q=0; q<(GLOBAL?1:Q);++q){
        auto me = getBlock(q);
        me.invert();
        setBlock(q, me);
      }
      return *this;
    }

    BlockProductKernel& transpose(){
      using std::swap;
      for(size_t q=0;q<(GLOBAL?1:Q);++q){
        auto me = getBlock(q);
        for(size_t row=0;row<P;++row){
          for(size_t col=row+1; col<P;++col){
            swap(me[row][col], me[col][row]);
          }
        }
        setBlock(q, me);
      }
      return *this;
    }

    BlockProductKernel& add(const BlockProductKernel& other){
      for(size_t i=0;i<_data.size();++i){
        for(size_t j=0; j<tileSize;++j){
          _data[i][j] += other._data[i][j];
        }
      }
      return *this;
    }

    BlockProductKernel& scale(const scalar_field_type& factor){
      for(auto& v : _data){
        for(auto& u : v){
          u *= factor;
        }
      }
      return *this;
    }

    BlockProductKernel& leftmultiply(const BlockProductKernel& other){
      for(size_t q=0; q<(GLOBAL?1:Q);++q){
        auto me = getBlock(q);
        auto you = other.getBlock(q);
        me.leftmultiply(you);
        setBlock(q, me);
      }
      return *this;
    }

    BlockProductKernel& rightmultiply(const BlockProductKernel& other){
      for(size_t q=0; q<(GLOBAL?1:Q);++q){
        auto me = getBlock(q);
        auto you = other.getBlock(q);
        me.rightmultiply(you);
        setBlock(q, me);
      }
      return *this;
    }

    field_type diagonal() const{
      field_type result;
      for(size_t q = 0; q<Q;++q){
        auto me = getBlock(GLOBAL?0:q);
        for(size_t p=0;p<P;++p){
          lane(q*P + p, result) = me[p][p];
        }
      }
      return result;
    }

    operator FieldMatrix<scalar_field_type,K,K>() const {
      FieldMatrix<scalar_field_type,K,K> result = 0.0;
      for(size_t q = 0; q<Q;++q){
        auto me = getBlock(GLOBAL?0:q);
        for(size_t p1=0;p1<P;++p1){
          for(size_t p2=0;p2<P;++p2){
            result[q*P + p1][q*P + p2] = me[p1][p2];
          }
        }
      }
      return result;
    }
    BlockProductKernel& operator=(const FieldMatrix<scalar_field_type, K,K>& o){
      for(size_t q = 0; q<(GLOBAL?1:Q);++q){
        FieldMatrix<scalar_field_type, P, P> me = 0.0;
        for(size_t p1=0;p1<P;++p1){
          for(size_t p2=0;p2<P;++p2){
            me[p1][p2] = o[q*P + p1][q*P + p2];
          }
        }
        setBlock(q, me);
      }
      return *this;
    }

  protected:
    FieldMatrix<scalar_field_type, P, P> getBlock(size_t q) const{
      FieldMatrix<scalar_field_type, P, P> mat = 0.0;
      for(size_t row=0;row<P;++row){
        for(size_t col = 0; col<P; ++col){
          mat[row][col] = lane(col%tileSize,_data[q*tiles*tiles + (row/tileSize)*tiles + col/tileSize][row%tileSize]);
        }
      }
      return mat;
    }

    void setBlock(size_t q, const FieldMatrix<scalar_field_type, P, P>& mat){
      for(size_t row=0;row<P;++row){
        for(size_t col = 0; col<P; ++col){
          lane(col%tileSize,_data[q*tiles*tiles + (row/tileSize)*tiles + col/tileSize][row%tileSize]) = mat[row][col];
        }
      }
    }
  };

#if HAVE_MPI
  // use the _data field in the communication
  template<class X, size_t P,bool GLOBAL>
  struct MPIData<BlockProductKernel<X,P, GLOBAL>>
    : MPIData<typename BlockProductKernel<X,P,GLOBAL>::data_type>{
    using BASE = MPIData<typename BlockProductKernel<X,P,GLOBAL>::data_type>;
    MPIData(BlockProductKernel<X,P,GLOBAL>& k)
      : BASE(k._data)
    {}
  };
#endif

  template<class X, size_t P=Dune::Simd::lanes<typename X::field_type>(), bool GLOBAL=false>
  class SequentialBlockProduct : public ScalarProduct<X>{
    using field_type = typename X::field_type;
    using scalar_field_type = Scalar<field_type>;
    static constexpr size_t K = lanes<field_type>();
    using Kernel = BlockProductKernel<X,P,GLOBAL>;

  public:
    virtual Future<Block<X>> idot(const X& x, const X& y) const override{
      Kernel result;
      result.bdot(x,y);
      return PseudoFuture<Block<X>>(result);
    }

    virtual Future<Block<X>> inormalize(X& x) const override{
      Kernel r;
      r = Lapack::tsqr(x, true, std::integral_constant<size_t,P>(), GLOBAL);
      return PseudoFuture<Block<X>>(std::move(r));
    }

  };

  template<class X, class C, size_t P=Dune::Simd::lanes<typename X::field_type>(), bool GLOBAL=false>
  class ParallelBlockProduct : public ParallelScalarProduct<X,C>{
    using field_type = typename X::field_type;
    using scalar_field_type = Scalar<field_type>;
    static constexpr size_t N = lanes<field_type>();
    using Kernel = BlockProductKernel<X,P,GLOBAL>;

  public:
    using ParallelScalarProduct<X,C>::ParallelScalarProduct;

    virtual Future<Block<X>> idot(const X& x, const X& y) const override{
      Kernel kernel;
      const std::set<size_t>& excludeSet = _communication.getExcludeSet();
      X tmp(x);
      for(auto idx : excludeSet)
        tmp[idx] = 0;
      kernel.bdot(tmp,y);
      return _communication.communicator().template iallreduce<std::plus<scalar_field_type>>(std::move(kernel));
    }

    virtual Future<Block<X>> inormalize(X& x) const override{
      // TSQR
      const std::set<size_t>& excludeSet = _communication.getExcludeSet();
      for(size_t idx : excludeSet){
        x[idx] = 0.0;
      }
      // normalize locally
      Kernel r, left;
      r = Lapack::tsqr(x, true, std::integral_constant<size_t,P>(), GLOBAL);

      if(_communication.communicator().size() == 1)
        return PseudoFuture<Block<X>>(r);
      static const TSQR_REDUCE::ReductionTree tree = TSQR_REDUCE::binaryReductionTree(_communication.communicator());
      TSQR_REDUCE::tsqr_pattern(&left,
                                &r,
                                1,
                                MPITraits<Kernel>::getType(),
                                [&](Kernel* a, Kernel* b, int count, Kernel* out){
                                  *out = Lapack::tsqr_reduce(*a,*b);
                                },
                                [&](Kernel* a, int ocunt, const Kernel* b){
                                  if(a == &left)
                                    left = *b;
                                  else
                                    a->rightmultiply(*b);
                                },
                                _communication.communicator(),
                                tree);
      X tmp(x);
      x=0.0;
      left.axpy(1.0, x, tmp);
      return PseudoFuture<Block<X>>(r);
    }

    virtual std::shared_ptr<GramSchmidt<X>> gramSchmidt() const override {
      std::string gramschmidt = _config.get("gramschmidt", "modified");
      if(gramschmidt == "modified")
        return std::make_shared<ModifiedGramSchmidt<X>>(*this);
      else if(gramschmidt == "classical")
        return std::make_shared<PipelinedHybridGramSchmidt<X>>(*this, 99999, _config.get("it", 1));
      else if(gramschmidt == "pipelinedhybrid")
        return std::make_shared<PipelinedHybridGramSchmidt<X>>(*this, _config.get("r", 1));
      else if(gramschmidt == "distributed"){
        typedef std::decay_t<decltype(_communication.communicator())> Comm;
        const std::set<size_t>& excludeSet = _communication.getExcludeSet();
        return std::make_shared<DistributedGramSchmidt<X, Comm, Kernel>>
          ([&](const X& x, const X& y){
             Kernel kernel;
             kernel.bdot(x,y);
             return kernel;
           },
            [&](X& x){
              for(size_t idx : excludeSet){
                x[idx] = 0.0;
              }
              typedef FieldMatrix<scalar_field_type, N, N> FM;
              FM r = Lapack::tsqr(x,true,std::integral_constant<size_t,P>());
              Kernel kernel;
              kernel = r;
              return kernel;
            },
            _communication.communicator());
      }else{
        DUNE_THROW(Dune::Exception, "Unknown Gram-Schmidt method");
      }
    }

  protected:
    using ParallelScalarProduct<X,C>::_communication;
    using ParallelScalarProduct<X,C>::_config;
  };

}

#endif
